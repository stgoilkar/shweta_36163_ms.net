﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicNs
{
    public class BasicCalculator
    {
        public int add(int x,int y)
        {
            return x + y;
        }

        public int sub(int x, int y)
        {
            return x - y;
        }

        public int mul(int x, int y)
        {
            return x * y;
        }

        public int div(int x, int y)
        {
            return x / y;
        }
    }
}

namespace TempratureNS
{
    public class TempratureConverter
    {
        public float FarenheitToCelcius(float x)
        {
            return ((x-32)*(5/9));
        }

        public float CelciusToFarenheit(float x)
        {
            return ((x*9/5)+32);
        }
    }
}