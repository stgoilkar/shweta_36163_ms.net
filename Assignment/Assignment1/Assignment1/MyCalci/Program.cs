﻿using BasicNs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TempratureNS;

namespace MyCalci
{
    class Program
    {
        static void Main(string[] args)
        {
            string isContinue = "y";

            do
            {
                int choice;
                Console.WriteLine("What operations you want to perform?");
                Console.WriteLine("1. Basic Arithmetic operation");
                Console.WriteLine("2. Temperature Convertion operation");
                choice = Convert.ToInt32(Console.ReadLine());


                BasicCalculator cal = new BasicCalculator();
                TempratureConverter temp = new TempratureConverter();


                if (choice==1)
                {
                    Console.WriteLine("Enter first Number");
                    int x = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("Enter second Number");
                    int y = Convert.ToInt32(Console.ReadLine());

                    int Airth;
                    int result=0;

                    Console.WriteLine("1. Addition");
                    Console.WriteLine("2. Subtraction");
                    Console.WriteLine("3. Multiplication");
                    Console.WriteLine("4. Division");

                    Airth = Convert.ToInt32(Console.ReadLine());


                    switch (Airth)
                    {
                        case 1:
                            result = cal.add(x, y);
                            break;
                        case 2:
                            result = cal.sub(x, y);
                            break;
                        case 3:
                            result = cal.mul(x, y);
                            break;
                        case 4:
                            result = cal.div(x, y);
                            break;
                        default:
                            Console.WriteLine("Enter correct output");
                            break;
                    }
                    Console.WriteLine(result);
                }
                else if (choice == 2)
                {
                    Console.WriteLine("Enter Temparature");
                    int x = Convert.ToInt32(Console.ReadLine());

                    int tempchoice;
                    float result=0;

                    Console.WriteLine("1. Celcius To Farenheit");
                    Console.WriteLine("2. Farenheit To Celcius");

                    tempchoice = Convert.ToInt32(Console.ReadLine());


                    switch (tempchoice)
                    {
                        case 1:
                            result = temp.CelciusToFarenheit(x);
                            break;
                        case 2:
                            result = temp.FarenheitToCelcius(x);
                            break;
                        default:
                            Console.WriteLine("Enter correct output");
                            break;
                    }
                    Console.WriteLine(result);
                }
                else
                {
                    Console.WriteLine("Wrong Input....Please enter the choice again");
                }

                Console.WriteLine("Do you want to Continue?y/n");
                isContinue = Console.ReadLine();

            } while (isContinue=="y");
            Console.ReadLine();
        }
    }
}
