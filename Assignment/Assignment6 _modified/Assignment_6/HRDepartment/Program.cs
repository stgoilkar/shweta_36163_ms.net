﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HRDepartment
{
    class Program
    {
        static Dictionary<int,Department> DeptList = new Dictionary<int, Department>();
        static Dictionary<int, Employee> EmpList = new Dictionary<int, Employee>();

        static void Main(string[] args)
        {
            #region Get Details
            getDeptDetails();
            //foreach (int key in DeptList.Keys)
            //{
            //    Console.WriteLine(DeptList[key]);
            //}

            getEmpDetails();
            #endregion

            #region Location with Single Department

            //List<string> Locationwithsingledept = LocationWithSingleDept();
            //foreach (string dept in Locationwithsingledept)
            //{
            //    Console.WriteLine(dept);
            //}
            #endregion

            #region  department names in which no employees
            // FindDeptsWithNoEmps();

            #endregion

            #region Calculate Total Salary
            //Calculate_Total_Salary();

            #endregion

            #region Get all Employees by Department

            //GetAllEmployeesByDept();

            #endregion

            #region Department wise Count of staff
            //DeptwiseStaffCount();
            /*      Dictionary<int, int> deptWiseStaff = DeptwiseStaffCount();
                  foreach (int key in deptWiseStaff.Keys)
                  {
                      Console.WriteLine("\n Deptartment Number : " + key + "\n" + " Count Of Staff : " + deptWiseStaff[key]);
                  }*/
            #endregion

            #region Department wise Avarage Salary
            //Dictionary<int, Double> deptWiseAvgSalary = DeptwiseAvgSal();
            //foreach (int key in deptWiseAvgSalary.Keys)
            //{
            //    Console.WriteLine("\n deptartment number : " + key + "\n" + " Avarage Salary : " + deptWiseAvgSalary[key]);
            //}
            #endregion

            #region Department Wise Minimum Salary
            //Dictionary<int, Double> deptwiseMinSal = DeptwiseMinSal();
            //foreach (int key in deptwiseMinSal.Keys)
            //{
            //    Console.WriteLine("\n deptartment number : " + key + "\n" + " Minimum Salary : " + deptwiseMinSal[key]);
            //}
            #endregion

            #region  employee names along with department names

            GetEmpInfo();

            #endregion
          

            Console.ReadLine();
        }


        #region get details
        private static void getEmpDetails()
        {
            FileStream fs = new FileStream(@"C:\CDAC\MS.NET\shweta_36163_ms.net\Assignment\Assignment6 _modified\emp.csv", FileMode.Open, FileAccess.Read);

            StreamReader reader = new StreamReader(fs);
            string empstring;
            while ((empstring = reader.ReadLine()) != null)
            {
                string[] empDetails = empstring.Split(',');
                Employee emp = new Employee();
                emp.EmpNo = int.Parse(empDetails[0]);
                emp.Name = empDetails[1];
                emp.Designation = empDetails[2];
                emp.Salary = double.Parse(empDetails[3]);
                emp.Commission = double.Parse(empDetails[4]);
                emp.DeptNo = int.Parse(empDetails[5]);
                EmpList.Add(emp.EmpNo, emp);
            }

            reader.Close();
            fs.Close();
        }

        static void getDeptDetails()
        {
            FileStream fs = new FileStream(@"C:\CDAC\MS.NET\shweta_36163_ms.net\Assignment\Assignment6 _modified\dept.csv", FileMode.Open, FileAccess.Read);

            StreamReader reader = new StreamReader(fs);
            string deptstring;
            while ((deptstring = reader.ReadLine()) != null)
            {
                string[] deptDetails = deptstring.Split(',');
                Department dept = new Department();
                dept.DeptNo = int.Parse(deptDetails[0]);
                dept.DeptName = deptDetails[1];
                dept.Location = deptDetails[2];
                DeptList.Add(dept.DeptNo, dept);
            }

            reader.Close();
            fs.Close();
        }
        #endregion
 
        #region Location With Single Department
        private static List<String> LocationWithSingleDept()
        {
            List<string> location = new List<string>();

            location = (from dept in DeptList
                        group dept by dept.Value.Location into loc
                        where loc.Count() == 1
                        select loc.Key).ToList();
            return location;
        }
        #endregion

        #region Find  department names in which no employees
        private static void FindDeptsWithNoEmps()
        {
            var deptName = (from d in DeptList join e in EmpList
                        on d.Value.DeptNo equals e.Value.DeptNo into joins
                        where joins.Count()==0
                        select new
                        {
                            dNo = d.Value.DeptNo,
                            deptName = d.Value.DeptName
                        }).ToList();

            foreach (var dept in deptName)
            {
                Console.WriteLine(dept.dNo + " "+dept.deptName);
            }
        }
        #endregion

        #region Calculate Total Salary
        private static void Calculate_Total_Salary()
        {
            var totalSalary = (from e in EmpList
                               select new{
                                empSalary = e.Value.Salary + e.Value.Commission,
                                empName =e.Value.Name}).ToList();
            foreach (var Salary in totalSalary)
            {
                Console.WriteLine("Name : "+Salary.empName +" , Net-Salary : "+Salary.empSalary);
            }
        }
        #endregion

        #region Get all Employees by Department
        private static void GetAllEmployeesByDept()
        {
            Console.WriteLine("Enter Deptartment Number : ");
            int deptnum = Convert.ToInt32(Console.ReadLine());
            var emp = (from e in EmpList
                       where e.Value.DeptNo==deptnum
                       select new
                       {
                           Empno = e.Value.EmpNo,
                           EmpName = e.Value.Name
                       }).ToList();

            foreach (var e in emp)
            {
                Console.WriteLine("Emp No: " +e.Empno + " , Name : " + e.EmpName);
            }
        }
        #endregion

        #region Department wise Count
        private static Dictionary<int,int> DeptwiseStaffCount()
        {
            /*        var result = EmpList.GroupBy(emp => emp.Value.DeptNo).Select(emp => new {
                        DeptNo = emp.Key,
                        EmpCount = emp.Count() });
                        foreach (var dept in result)
                        {
                            Console.WriteLine(dept);
                        }*/

            var countStaff = (from d in DeptList 
                              join e in EmpList
                              on d.Value.DeptNo equals e.Value.DeptNo into joins
                              select new
                              {
                                  deptno = d.Value.DeptNo,
                                  count = joins.Count()
                              });
            Dictionary<int, int> staff = new Dictionary<int, int>();
            foreach (var arr in countStaff)
            {
                staff.Add(arr.deptno, arr.count);
            }

            return staff;
        }
        #endregion

        #region department wise Avarage Salary
        private static Dictionary<int, double> DeptwiseAvgSal()
        {
            Dictionary<int, double> avgSal = (from d in DeptList
                                              join e in EmpList
                                                on d.Value.DeptNo equals e.Value.DeptNo into joins
                                              select new
                                              {
                                                  deptno = d.Value.DeptNo,
                                                  avgSalary = joins.Count() > 0 ? joins.Average(e => e.Value.Salary) : 0
                                              }).ToDictionary(g => g.deptno, g => g.avgSalary);

            return avgSal;
        }
        #endregion

        #region Department wise Minimum salary 
        private static Dictionary<int, double> DeptwiseMinSal()
        {
            Dictionary<int, double> minSal = (from d in DeptList
                                              join e in EmpList
                                              on d.Value.DeptNo equals e.Value.DeptNo into joins
                                              select new
                                              {
                                              deptno = d.Value.DeptNo,
                                              minSalary = joins.Count() > 0 ? joins.Min(e => e.Value.Salary) : 0
                                              }).ToDictionary(g => g.deptno, g => g.minSalary);
            return minSal;
        }
        #endregion

        #region  employee names along with department names
        private static void GetEmpInfo()
        {
            var empnamewithDept = (from e in EmpList
                                   join d in DeptList
                                   on e.Value.DeptNo equals d.Value.DeptNo
                                   select new
                                   {
                                       EmployeeNo=e.Value.EmpNo,
                                       EmployeeName=e.Value.Name,
                                       deptName=d.Value.DeptName
                                   });
            foreach (var emp in empnamewithDept)
            {
                Console.WriteLine("EmployeeNo : " + emp.EmployeeNo + " , EmployeeName : " + emp.EmployeeName + " , DeptarmentName : " + emp.deptName);
            }
        }
        #endregion
    }
}
