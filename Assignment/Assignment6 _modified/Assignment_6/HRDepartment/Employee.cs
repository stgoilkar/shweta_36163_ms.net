﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRDepartment
{
    public class Employee
    {
        private int _EmpNo;
        private string _Name;
        private string _Designation;
        private double _Salary;
        private double _Commission;
        private int _DeptNo;

        #region Constructor
        public Employee()
        {
            this.EmpNo = 0;
            this.Name = "";
            this.Designation = "";
            this.Salary = 0;
            this.Commission = 0;
            this.DeptNo = 0;
        }

        public Employee(int EmpNo,string Name,string Designation, double Salary,double Commission,int DeptNo)
        {
            this.EmpNo = EmpNo;
            this.Name = Name;
            this.Designation = Designation;
            this.Salary = Salary;
            this.Commission = Commission;
            this.DeptNo = DeptNo;
        }
        #endregion

        #region Getter Setter
        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }


        public double Commission
        {
            get { return _Commission; }
            set { _Commission = value; }
        }


        public double Salary
        {
            get { return _Salary; }
            set { _Salary = value; }
        }


        public string Designation
        {
            get { return _Designation; }
            set { _Designation = value; }
        }


        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int EmpNo
        {
            get { return _EmpNo; }
            set { _EmpNo = value; }
        }
        #endregion

        #region Method
        public override string ToString()
        {
            return "\n Employeess......."+"\n"+ " Employee Number : " + this.EmpNo + "\n" + " Employee Name : " + this.Name + "\n" + " Designation : " + this.Designation + "\n" + " Salary : " + this.Salary + "\n" + " Commission : " + this.Commission + "\n" + " Department Number : " + this.DeptNo ;
        }
        #endregion
    }
}
