﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRDepartment
{
    public class Department
    {
        private int _DeptNo;
        private string _DeptName;
        private string _Location;

        #region Constructor
        public Department()
        {
            this.DeptNo = 0;
            this.DeptName = "";
            this.Location = "";
        }

        public Department(int DeptNo, string DeptName, string Location)
        {
            this.DeptNo = DeptNo;
            this.DeptName = DeptName;
            this.Location = Location;
        }
        #endregion

        #region Getter Setter
        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }


        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }


        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }
        #endregion

        #region Method
        public override string ToString()
        {
            return "Departments......." + "\n" + " Department Number : " + this.DeptNo + "\n" + " Department Name : " + this.DeptName + "\n" + " Location : " + this.Location + "\n" ;
        }
        #endregion
    }
}
