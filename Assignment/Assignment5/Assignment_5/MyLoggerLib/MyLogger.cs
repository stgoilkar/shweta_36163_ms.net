﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MyLoggerLib
{
    public class MyLogger
    {
        private static MyLogger logger = new MyLogger();
        private MyLogger()
        {
            Console.WriteLine("Logger Object created");
        }

        public static MyLogger CurrentSingleton
        {
            get
            {
                return logger;
            }
        }

        public void Log(string message)
        {
            FileStream fs = new FileStream(@"C:\CDAC\MS.NET\shweta_36163_ms.net\Assignment\Assignment5\AllLogs\Log" + DateTime.Now.ToString("ddMMyyyy") + ".csv",
                                            FileMode.Append,
                                            FileAccess.Write);

            StreamWriter writer = new StreamWriter(fs);
            string msg=DateTime.Now.ToString() + " , " + message;

            writer.WriteLine(msg);
            Console.WriteLine(msg);
            fs.Flush();
            writer.Close();
            fs.Close();
        }

    }
}
