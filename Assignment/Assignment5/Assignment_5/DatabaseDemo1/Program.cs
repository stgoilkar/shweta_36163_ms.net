﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyLoggerLib;

namespace DatabaseDemo1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                MyLogger.CurrentSingleton.Log("Main Started");
                Console.WriteLine("Enter the database you want to select");
                Console.WriteLine("1: SQL server, 2: Oracle");
                int choice = Convert.ToInt32(Console.ReadLine());

                Report report = new Report();
                Database db = report.GetObjectback(choice);

                Console.WriteLine("Enter the operation you want to perform");
                Console.WriteLine("1: Insert, 2: Update, 3:Delete");
                int operation = Convert.ToInt32(Console.ReadLine());

                switch (operation)
                {
                    case 1:
                        MyLogger.CurrentSingleton.Log("Logging to insert.....");
                        db.insert();
                        break;
                    case 2:
                        MyLogger.CurrentSingleton.Log("Logging to update....");
                        db.update();
                        break;
                    case 3:
                        MyLogger.CurrentSingleton.Log("Logging to Delete....");
                        db.delete();
                        break;
                    default:
                        Console.WriteLine("Invalid Choice");
                        break;
                }
            }
            catch (Exception ex)
            {
                MyLogger.CurrentSingleton.Log("Main Failed!! Details:  " + ex.Message);
            }

            MyLogger.CurrentSingleton.Log("Main Completed");
            Console.ReadLine();
        }
    }

    public class Report
    {
        public Database GetObjectback(int choice)
        {
            if (choice == 1)
            {
                MyLogger.CurrentSingleton.Log("Inside SQL...");
                return new SQL();
            }
            else
            {
                MyLogger.CurrentSingleton.Log("Inside Oracle...");
                return new oracle();
            }
        }
    }

    public abstract class Database
    {
        public abstract void insert();
        public abstract void update();
        public abstract void delete();
    }

    public class SQL : Database
    {
        public override void insert()
        {
            Console.WriteLine("SQL insert");
        }

        public override void update()
        {
            Console.WriteLine("SQL update");
        }

        public override void delete()
        {
            Console.WriteLine("SQL delete");
        }
    }

    public class oracle : Database
    {
        public override void insert()
        {
            Console.WriteLine("oracle insert");
        }

        public override void update()
        {
            Console.WriteLine("oracle update");
        }

        public override void delete()
        {
            Console.WriteLine("oracle delete");
        }
    }

}
