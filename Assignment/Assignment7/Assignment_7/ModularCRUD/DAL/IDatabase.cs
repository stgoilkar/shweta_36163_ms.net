﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModularCRUD.POCO;

namespace ModularCRUD.DAL
{
    public interface IDatabase
    {
        List<Emp> Select();
        int Insert(Emp emp);
        int Update(Emp emp);
        int Delete(int no);
    }
}
