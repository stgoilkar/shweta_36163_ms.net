﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModularCRUD.POCO;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace ModularCRUD.DAL
{
    public class sqlServer : IDatabase
    {
        public List<Emp> Select()
        {
            List<Emp> allEmps = new List<Emp>();

            string constr = ConfigurationManager.ConnectionStrings["connString"].ToString();
            SqlConnection conn = new SqlConnection(constr);

            string query = "Select * from Emp";
            SqlCommand cmd = new SqlCommand(query, conn);

            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Emp emp = new Emp();
                {
                    emp.No = Convert.ToInt32(reader["Id"]);
                    emp.Name = reader["Name"].ToString();
                    emp.Address = reader["Address"].ToString();
                }
                allEmps.Add(emp);
            }

            conn.Close();
            return allEmps;
        }

        public int Insert(Emp emp)
        {
            SqlConnection conn = null;
            int rowsAffected = 0;
            try
            {
                string Constr = ConfigurationManager.ConnectionStrings["connString"].ToString();

                conn = new SqlConnection(Constr);

                SqlCommand cmd = new SqlCommand("spInsert", conn);

                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter parameter1 = new SqlParameter("@No", SqlDbType.Int);
                parameter1.Value = Convert.ToInt32(emp.No);

                SqlParameter parameter2 = new SqlParameter("@Name", SqlDbType.VarChar, 50);
                parameter2.Value = emp.Name;

                SqlParameter parameter3 = new SqlParameter("@Address", SqlDbType.VarChar,50);
                parameter3.Value = emp.Address;

                cmd.Parameters.Add(parameter1);
                cmd.Parameters.Add(parameter2);
                cmd.Parameters.Add(parameter3);

                conn.Open();
                rowsAffected= cmd.ExecuteNonQuery();
                Console.WriteLine("Record inserted Successfully.....");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Record Insertion Failed using Stored Procedure...");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return rowsAffected;
        }

        public int Update(Emp emp)
        {
            SqlConnection conn = null;
            int rowsAffected = 0;
            try
            {
                string Constr = ConfigurationManager.ConnectionStrings["connString"].ToString();

                conn = new SqlConnection(Constr);

                SqlCommand cmd = new SqlCommand("spUpdate", conn);

                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter parameter1 = new SqlParameter("@No", SqlDbType.Int);
                parameter1.Value = Convert.ToInt32(emp.No);

                SqlParameter parameter2 = new SqlParameter("@Name", SqlDbType.VarChar, 50);
                parameter2.Value = emp.Name;

                SqlParameter parameter3 = new SqlParameter("@Address", SqlDbType.VarChar,50);
                parameter3.Value = emp.Address;

                cmd.Parameters.Add(parameter1);
                cmd.Parameters.Add(parameter2);
                cmd.Parameters.Add(parameter3);

                conn.Open();
                rowsAffected = cmd.ExecuteNonQuery();
                Console.WriteLine("Record updated Successfully.....");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Record Updation Failed using Stored Procedure...");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return rowsAffected;
        }

        public int Delete(int No)
        {
            SqlConnection conn = null;
            int rowsAffected = 0;
            try
            {
                string constr = ConfigurationManager.ConnectionStrings["connString"].ToString();
                conn = new SqlConnection(constr);

                SqlParameter parameter = new SqlParameter("@No", SqlDbType.Int);
                parameter.Value = No;

                SqlCommand cmd = new SqlCommand("delete from Emp where Id=@No", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(parameter);
                conn.Open();

                rowsAffected=cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                Console.WriteLine("Record Deletion Failed....");
                Console.WriteLine("Technical Details : "+ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return rowsAffected;
        }



    }
}
