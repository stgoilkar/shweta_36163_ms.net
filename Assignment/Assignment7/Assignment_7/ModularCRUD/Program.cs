﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModularCRUD.DAL;
using ModularCRUD.POCO;

namespace ModularCRUD
{
    class Program
    {
        static void Main(string[] args)
        {
            dbFactory dbFactory = new dbFactory();
            IDatabase db = dbFactory.GetDatabase();

            string iscontinue = "y";

            do
            {
                Console.WriteLine("1 : Read Data , 2 : Insert Data , 3 : Update Data , 4 : Delete Data ");
                int choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        #region Read
                        var Data = db.Select();
                        foreach (var item in Data)
                        {
                            Console.WriteLine("Id : " + item.No + " , Name : " + item.Name + " , Address : " + item.Address);
                        } 
                        #endregion
                        break;
                    case 2:
                        #region Insert
                        Emp emp = new Emp();
                        Console.WriteLine("Enter the Id : ");
                        emp.No = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Enter the Name : ");
                        emp.Name = Console.ReadLine();
                        Console.WriteLine("Enter the Address : ");
                        emp.Address = Console.ReadLine();

                        int rowsAffected = db.Insert(emp);
                        Console.WriteLine("Rows Affected = " + rowsAffected); 
                        #endregion
                        break;
                    case 3:
                        #region Update
                        Emp emp1 = new Emp();
                        Console.WriteLine("Enter the Id : ");
                        emp1.No = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Enter the Name : ");
                        emp1.Name = Console.ReadLine();
                        Console.WriteLine("Enter the Address : ");
                        emp1.Address = Console.ReadLine();

                        int rowAffected = db.Update(emp1);
                        Console.WriteLine("Rows Affected = " + rowAffected);
                        #endregion
                        break;
                    case 4:
                        #region Delete
                        Console.WriteLine("Enter the Id : ");
                        int No = Convert.ToInt32(Console.ReadLine());

                        int RowAffected = db.Delete(No);
                        Console.WriteLine("Rows Affected = " + RowAffected);
                        #endregion
                        break;
                    default:
                        Console.WriteLine("Please Enter Valid Operation... ");
                        break;
                }
                Console.WriteLine("Do you want to Continue ? y/n");
                iscontinue = Console.ReadLine();
            } while (iscontinue == "y");
            Console.ReadLine();
        }
    }
}
