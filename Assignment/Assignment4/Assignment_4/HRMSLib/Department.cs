﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Department
    {
        private int _deptNo;
        private string _deptName;
        private string _location;

        #region Constructor
        public Department()
        {
            this.DeptNo = 0;
            this.DeptName = "";
            this.Location = "";
        }

        public Department(int deptNo,string deptName,string location)
        {
            this.DeptNo = deptNo;
            this.DeptName = deptName;
            this.Location = location;
        }
        #endregion

        #region Getter Setter
        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }


        public string DeptName
        {
            get { return _deptName; }
            set { _deptName = value; }
        }


        public int DeptNo
        {
            get { return _deptNo; }
            set { _deptNo = value; }
        }
        #endregion

        #region Method
        public override string ToString()
        {
            return "Deptartment No : " + Convert.ToString(this.DeptNo) + " , Department Name : " + this.DeptName + " , Location : " + this.Location;
        }
        #endregion
    }
}
