﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMSLib;

namespace HRMSLib
{
    public class Person
    {
        #region private member
        private string _Name;
        private bool _gender;
        private Date _birth;
        private string _address;
        private string _EmailId;



        #endregion

        #region Constructor

        public Person()
        {
            this.Name = "";
            this._gender = true;
            this.birth = new Date();
            this.address = "";
            this.EmailId = "";
        }

        public Person(string name,bool gender,Date birth,string address,string EmailId)
        {
            this._Name = name;
            this._gender = gender;
            this._birth = birth;
            this._address = address;
            this.EmailId = EmailId;
        }
        #endregion

        #region getter

        public string EmailId
        {
            get { return _EmailId; }
            set { _EmailId = value; }
        }

        public string address
	{
		get { return _address;}
		protected set { _address = value;}
	}


	public Date birth
	{
		get { return _birth;}
        protected set { _birth = value;}
	}


     protected void setGender(bool gender) { this._gender = gender;}
	public string getGender()
        {
            if (_gender == true)
            {
                return "female";
            }
            else
            {
                return "male";
            }
        }

        

    public string Name
	{
		get { return _Name;}
        protected set { _Name = value;}
	}
        #endregion


        #region Method

        public int Age()
        {
            int d = DateTime.Now.Day;
            int m = DateTime.Now.Month;
            int y = DateTime.Now.Year;

            Date currentDate = new Date(y, m, d);
            return (birth - currentDate);
        }

        public override string ToString()
        {
            return this.Name + " gender is " +(this.getGender())+ " with birth date " + this.birth + " and address is "+this.address + " , age = " + Age()+ " ,  Email : " +EmailId;
        }

        #endregion
    }
}
