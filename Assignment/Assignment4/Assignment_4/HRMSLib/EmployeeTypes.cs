﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    //Write a enum EmployeeTypes.cs with following members:
    //    Trainee, Permanent, Temporary
    public enum EmployeeTypes
    {
        Trainee, Permanent, Temporary
    }
}
