﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class exception : Exception
    {
        public exception(string msg) : base(msg)
        {

        }
    }
}
