﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Employee :Person
    {
        private static int count = 1;
        private int _id;
        private double _salary;
        private double _hra;
        private double _da;
        private string _designation;
        private EmployeeTypes _empType;
        private string _Passcode;
        private int _DeptNo;
        private Date _HireDate;



        #region constructor

        public Employee():base()
        {
            this.id = count++;
            this.salary = 0.0;
            this.hra = 4 * this.salary / 10;
            this.da = this.salary / 10;
            this.designation = "";
            this.empType = 0;
            this.Passcode = "";
            this.DeptNo = 0;
            this.HireDate = new Date();
        }

        public Employee(string name, bool gender, Date birth, string address,string emailId,double salary, string designation, EmployeeTypes empType,string Passcode,int DeptNo,Date date):base(name,gender,birth,address,emailId)
        {
            this.id = count ++;
            this.salary = salary;
            this.hra = salary * 40 / 100;
            this.da = salary * 10 / 100;
            this.designation = designation;
            this.empType = empType;
            this.Passcode = Passcode;
            this.DeptNo = DeptNo;
            this.HireDate = date;
        }
        #endregion

        #region getter setter

        public Date HireDate
        {
            get { return _HireDate; }
            set { _HireDate = value; }
        }


        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }

        public string Passcode
        {
            get { return _Passcode; }
            set { _Passcode = value; }
        }

        public EmployeeTypes empType
        {
            get { return _empType; }
            set { _empType = value; }
        }


        public string designation
        {
            get { return _designation; }
            set { _designation = value; }
        }


        public double da
        {
            get { return _da; }
            protected set { _da = value; }
        }


        public double hra
        {
            get { return _hra; }
            protected set { _hra =value; }
        }


        public double salary
        {
            get { return _salary; }
            set { _salary = value; }
        }


        public int id
        {
            get { return _id; }
            set { _id = value; }
        }
        #endregion

        #region method
        public virtual double NetSalary()
        {
            return this.salary + this.hra + this.da;
        } 

        public override string ToString()
        {
            return base.ToString() + " Employee Information : " + " id = " + Convert.ToString(this.id) + " , salary = " + Convert.ToString(this.salary) + " ,  HRA = " + Convert.ToString(this.hra) + " ,  DA = " + Convert.ToString(this.da) + " ,  designation = " + this.designation + " , EmployeeType =  " + this.empType + " , Passcode = " +this.Passcode + " , Department No = " +this.DeptNo + " , HireDate = " + this.HireDate;
        }
        #endregion

    }
}
