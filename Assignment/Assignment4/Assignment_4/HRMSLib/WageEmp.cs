﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class WageEmp:Employee
    {
        private int _hour;
        private int _rate;

        public WageEmp() : base()
        {
            this.hour = 0;
            this.rate = 0;
        }

        public WageEmp(string name, bool gender, Date birth, string address, string emailId, EmployeeTypes empType, string Passcode, int DeptNo, Date date, int hour,int rate) : base(name, gender, birth, address,emailId, 0, "Wage", empType, Passcode,  DeptNo,  date)
        {
            this.hour = hour;
            this.rate = rate;
        }

        public int rate
        {
            get { return _rate; }
            set { _rate = value; }
        }


        public int hour
        {
            get { return _hour; }
            set { _hour = value; }
        }

        public override double NetSalary()
        {
            return this.hour*this.rate;
        }
        public override string ToString()
        {
            return base.ToString() + " Hours : " + Convert.ToString(this.hour) +" , Rate : " + Convert.ToString(this.rate);
        }
    }
}
