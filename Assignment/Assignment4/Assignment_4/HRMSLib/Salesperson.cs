﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Salesperson:Employee
    {
        private double _commission;

        public Salesperson():base()
        {
            this.commission = 0;
        }

        public Salesperson(string name, bool gender, Date birth, string address, string emailId, double salary, EmployeeTypes empType, string Passcode, int DeptNo, Date date, double commission):base(name,  gender,  birth,  address,emailId,  salary, "Salesperson", empType,  Passcode,  DeptNo,  date)
        {
            this.commission = commission;
        }

        public double commission
        {
            get { return _commission; }
            set { _commission = value; }
        }

        public override double NetSalary()
        {
            return base.NetSalary()+this.commission;
        }

        public override string ToString()
        {
            return base.ToString()+ " Commission =  "+ Convert.ToString(this.commission);
        }
    }
}
