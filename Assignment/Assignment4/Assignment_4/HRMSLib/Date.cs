﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMSLib;

namespace HRMSLib
{
	public class Date
	{
		#region private member
		private int _day;
		private int _month;
		private int _year;
		#endregion

		#region Constructor
		public Date()
		{
			this.year = 1900;
			this.month = 1;
			this.day = 1;
		}

		public Date(int year, int month, int day)
        {
			this.year = year;
			this.month = month;
			this.day = day;
        }
		#endregion

		#region Getter setter
		public int year
		{
			get
			{
				return _year;
			}
			set
			{
				if (1900 <= value && value < 2100)
				{
					_year = value;
				}
				else
				{
					throw new exception("Enter year between 1900 to 2100 only");
				}
			}
		}


		public int month
		{
			get 
			{ 
				return _month; 
			}
			set
			{
				if (1 <= value && value <= 12)
				{
					_month = value;
				}
				else
				{
					throw new exception("Enter month between 1 to 12 only");
				}
			}
		}


		public int day
		{
			get 
			{ 
				return _day; 
			}
            set {
				if (value <= 31 && value > 0)
				{
					if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
					{
						if (value <= 31 && value > 0)
						{
							_day = value;
						}
						else
						{
							throw new exception("Enter day between 1 to 31 only");
						}
					}
					else if (month == 4 || month == 6 || month == 9 || month == 11)
					{
						if (value <= 30 && value > 0)
						{
							_day = value;
						}
						else
						{
							throw new exception("Enter day between 1 to 30 only");
						}
					}
					else if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
					{
						if (value <= 29 && value > 0)
						{
							_day = value;
						}
						else
						{
							throw new exception("Enter day between 1 to 29 only");
						}
					}
					else
					{
						if (value <= 28 && value > 0)
						{
							_day = value;
						}
						else
						{
							throw new exception("Enter day between 1 to 28 only");
						}
					}
                }
                else
                {
					throw new exception("Enter valid Date");
				}
			}
		}

		#endregion

		public override string ToString()
        {
			return (this.day + "/" + this.month + "/" + this.year);
        }

		public static int operator - (Date d1,Date d2)
        {
            if (d1.month > d2.month)
            {
			return Math.Abs(d1.year - d2.year);
            }
            else
            {
				return Math.Abs(d1.year - d2.year) - 1;
            }
        }
	}
} 