﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMSLib;
using System.Collections;

namespace ConsoleApp
{
    class Program
    {
        static List<Employee> emp = new List<Employee>();
        static List<Salesperson> salespeople = new List<Salesperson>();
        static List<WageEmp> wage = new List<WageEmp>();
        static List<Department> departments = new List<Department>();

        static void Main(string[] args)
        {
            string iscontinue = "y";

            do
            {
                Console.WriteLine("Select the Option Below");
                Console.WriteLine("1 : Accept  , 2 : Print ");
                int choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        accept();
                        break;
                    case 2:
                        print();
                        break;
                    default:
                        Console.WriteLine("Enter correct Option");
                        break;
                }

                Console.WriteLine("Do you want to Continue?y/n");
                iscontinue = Console.ReadLine();
            } while (iscontinue == "y");

            Console.ReadLine();
        }

        public static void print()
        {

            string iscontinue = "y";
            ArrayList arr = new ArrayList();
            do
            {
                Console.WriteLine("Select the Option Below");
                Console.WriteLine("1 : Salesperson , 2 : Wage , 3 : Other Employee , 4 : Department");
                int choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        Console.WriteLine("----------------------------------------------------------------------------------------------------");
                        Console.WriteLine("You entered below data for Salesperson");
                        foreach (Salesperson salesperson in salespeople)
                        {
                            Console.WriteLine(salesperson);
                            Console.WriteLine("Net Salary is ");
                            Console.WriteLine(salesperson.NetSalary());
                        }
                        break;
                    case 2:
                        Console.WriteLine("----------------------------------------------------------------------------------------------------");
                        Console.WriteLine("You entered below data for Wages");
                        foreach (WageEmp wageEmp in wage)
                        {
                            Console.WriteLine(wageEmp);
                            Console.WriteLine("Net Salary is ");
                            Console.WriteLine(wageEmp.NetSalary());
                        }
                        break;
                    case 3:
                        Console.WriteLine("----------------------------------------------------------------------------------------------------");
                        Console.WriteLine("You entered below data for Employee");
                        foreach (Employee employee in emp)
                        {
                            Console.WriteLine(employee);
                            Console.WriteLine("Net Salary is ");
                            Console.WriteLine(employee.NetSalary());
                        }
                        break;
                    case 4:
                        Console.WriteLine("----------------------------------------------------------------------------------------------------");
                        Console.WriteLine("You entered below data for Department");
                        foreach (Department department in departments)
                        {
                            Console.WriteLine(department);
                        }
                        break;
                    default:
                        Console.WriteLine("Enter correct Option");
                        break;
                }

                Console.WriteLine("Do you want to Continue with printing data?y/n");
                iscontinue = Console.ReadLine();
            } while (iscontinue == "y");
        }

        public static void accept()
        {
            string iscontinue = "y";
            ArrayList arr = new ArrayList();
            do
            {
                Console.WriteLine("Select the Option Below");
                Console.WriteLine("1 : Salesperson , 2 : Wage , 3 : Other Employee , 4 : Department");
                int choice = Convert.ToInt32(Console.ReadLine());
                try
                {
                    switch (choice)
                    {
                        case 1:
                            salespeople.Add(SalespersonSelect());
                            break;
                        case 2:
                            wage.Add(WageEmpSelect());
                            break;
                        case 3:
                            emp.Add(empSelect());
                            break;
                        case 4:
                            departments.Add(departmentSelect());
                            break;
                        default:
                            Console.WriteLine("Enter correct Option");
                            break;
                    }
                }catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                Console.WriteLine("Do you want to Continue with accepting data?y/n");
                iscontinue = Console.ReadLine();
            } while (iscontinue == "y");
        }

        public static Employee empSelect()
        {
            Console.WriteLine("enter your name");
            string name = Console.ReadLine();

            Console.WriteLine("Enter your Gender.... f/m");
            bool gender = (Console.ReadLine()=="f"?true:false);

            Console.WriteLine("Enter your Address");
            string address = Console.ReadLine();

            Console.WriteLine("Enter birth");
            Date d1 = new Date();
 
                Console.WriteLine("Enter Year");
                d1.year = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Month");
                d1.month = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Day");
                d1.day = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(d1.ToString());

            Console.WriteLine("Enter your Email Id");
            string email = Console.ReadLine();

            Console.WriteLine("Enter the salary");
            double salary = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Enter the Designation");
            string designation = Console.ReadLine();

            Console.WriteLine("Enter the Passcode");
            string passcode = Console.ReadLine();

            Console.WriteLine("Enter the Department Number");
            int deptNo = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Select Employee type");
           Console.WriteLine("0 : Trainee, 1 : Permanent, 2 : Temporary");
           EmployeeTypes empType = (EmployeeTypes)Convert.ToInt32(Console.ReadLine());


            Console.WriteLine("Enter HireDate");
            Date d2 = new Date();

                Console.WriteLine("Enter Year");
                d2.year = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Month");
                d2.month = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Day");
                d2.day = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(d2.ToString());

            Employee e = new Employee(name,gender , new Date(d1.year, d1.month, d1.day), address,email, salary,designation, empType, passcode,deptNo,new Date(d2.year,d2.month,d2.day));

            return e;
        }

        public static Salesperson SalespersonSelect()
        {
            Console.WriteLine("enter your name");
            string name = Console.ReadLine();

            Console.WriteLine("Enter your Gender.... f/m");
            bool gender = (Console.ReadLine() == "f" ? true : false);

            Console.WriteLine("Enter your Address");
            string address = Console.ReadLine();

            Console.WriteLine("Enter birth");
            Date d1 = new Date();
   
                Console.WriteLine("Enter Year");
                d1.year = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Month");
                d1.month = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Day");
                d1.day = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(d1.ToString());


            Console.WriteLine("Enter your Email Id");
            string email = Console.ReadLine();

            Console.WriteLine("Enter the salary");
            double salary = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Enter the Commission");
            double commission =Convert.ToDouble(Console.ReadLine());


            Console.WriteLine("Enter the Passcode");
            string passcode = Console.ReadLine();

            Console.WriteLine("Enter the Department Number");
            int deptNo = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Select Employee type");
            Console.WriteLine("0 : Trainee, 1 : Permanent, 2 : Temporary");
            EmployeeTypes empType = (EmployeeTypes)Convert.ToInt32(Console.ReadLine());


            Console.WriteLine("Enter HireDate");
            Date d2 = new Date();
     
                Console.WriteLine("Enter Year");
                d2.year = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Month");
                d2.month = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Day");
                d2.day = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(d2.ToString());



            Salesperson s = new Salesperson(name, gender, new Date(d1.year, d1.month, d1.day), address,email, salary,empType,passcode,deptNo,new Date(d2.year,d2.month,d2.day), commission);

            return s;
        }

        public static WageEmp WageEmpSelect()
        {

            Console.WriteLine("enter your name");
            string name = Console.ReadLine();

            Console.WriteLine("Enter your Gender.... f/m");
            bool gender = (Console.ReadLine() == "f" ? true : false);

            Console.WriteLine("Enter your Address");
            string address = Console.ReadLine();

            Console.WriteLine("Enter birth");
            Date d1 = new Date();
  
                Console.WriteLine("Enter Year");
                d1.year = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Month");
                d1.month = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Day");
                d1.day = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(d1.ToString());


            Console.WriteLine("Enter your Email Id");
            string email = Console.ReadLine();

            Console.WriteLine("Enter the Passcode");
            string passcode = Console.ReadLine();

            Console.WriteLine("Enter the Department Number");
            int deptNo = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the Hour");
            int hour = Convert.ToInt32(Console.ReadLine());


            Console.WriteLine("Enter the Rate");
            int rate = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Select Employee type");
            Console.WriteLine("0 : Trainee, 1 : Permanent, 2 : Temporary");
            EmployeeTypes empType = (EmployeeTypes)Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter HireDate");
            Date d2 = new Date();
   
                Console.WriteLine("Enter Year");
                d2.year = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Month");
                d2.month = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Day");
                d2.day = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(d2.ToString());



            WageEmp w = new WageEmp(name, gender, new Date(d1.year, d1.month, d1.day), address,email, empType, passcode, deptNo, new Date(d2.year, d2.month, d2.day), hour,rate);
            
            return w;
        }

        public static Department departmentSelect()
        {
            Console.WriteLine("enter department No");
            int deptno = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter department Name");
            string name = Console.ReadLine();
            Console.WriteLine("enter Location");
            string location = Console.ReadLine();

            Department d = new Department(deptno, name, location);
            return d;
        }
    }
 }
