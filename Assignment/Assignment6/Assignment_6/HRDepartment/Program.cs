﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HRDepartment
{
    class Program
    {
        static Dictionary<int,Department> DeptList = new Dictionary<int, Department>();
        static Dictionary<int, Employee> EmpList = new Dictionary<int, Employee>();

        static void Main(string[] args)
        {
            #region Get Details
            getDeptDetails();
            //foreach (int key in DeptList.Keys)
            //{
            //    Console.WriteLine(DeptList[key]);
            //}

            getEmpDetails();
            #endregion

            #region Location with Single Department
            //List<string> arr = LocationWithSingleDept();
            //foreach (string list in arr)
            //{
            //    Console.WriteLine(list);
            //}
            #endregion

            #region Find Employee with no Departments
            //List<string> DeptsWithNoEmps = FindDeptsWithNoEmps();
            //foreach (string dept in DeptsWithNoEmps)
            //{
            //    Console.WriteLine(dept);
            //}
            #endregion

            #region Calculate Total Salary
            //Dictionary<int,double> netSalary= Calculate_Total_Salary();
            //foreach (int netSalarykey in netSalary.Keys)
            //{
            //    Console.WriteLine(EmpList[netSalarykey]);
            //    Console.WriteLine(" Net Salary : " + netSalary[netSalarykey]);
            //} 
            #endregion

            #region Get all Employees by Department
            //Console.WriteLine("Enter Department Number : ");
            //int DeptNo = Convert.ToInt32(Console.ReadLine());
            //GetAllEmployeesByDept(DeptNo);

            #endregion

            #region Department wise Count of staff
            //Dictionary<int, int> deptWiseStaff = DeptwiseStaffCount();
            //foreach (int key in deptWiseStaff.Keys)
            //{
            //    Console.WriteLine("\n Deptartment Number : " +key +"\n" + " Count Of Staff : " + deptWiseStaff[key]);
            //} 
            #endregion

            #region Department wise Avarage Salary
            //Dictionary<int, Double> deptWiseAvgSalary = DeptwiseAvgSal();
            //foreach (int key in deptWiseAvgSalary.Keys)
            //{
            //    Console.WriteLine("\n deptartment number : " + key + "\n" + " Avarage Salary : " + deptWiseAvgSalary[key]);
            //} 
            #endregion

            #region Department Wise Minimum Salary
            Dictionary<int, Double> deptwiseMinSal = DeptwiseMinSal();
            foreach (int key in deptwiseMinSal.Keys)
            {
                Console.WriteLine("\n deptartment number : " + key + "\n" + " Minimum Salary : " + deptwiseMinSal[key]);
            } 
            #endregion

            Console.ReadLine();
        }

        #region Location With Single Department
        private static List<String> LocationWithSingleDept()
        {
            List<string> location = new List<string>();

            foreach (int key in DeptList.Keys)
            {
                int count = 0;
                string stringlocation = DeptList[key].Location;
                foreach (int locationkey in DeptList.Keys)
                {
                    if (DeptList[locationkey].Location == stringlocation)
                    {
                        count++;
                    }
                }
                if (count == 1)
                {
                    location.Add(stringlocation);
                }
            }
            return location;
        } 
        #endregion

        #region get details
        private static void getEmpDetails()
        {
            FileStream fs = new FileStream(@"C:\CDAC\MS.NET\shweta_36163_ms.net\Assignment\Assignment6\emp.csv", FileMode.Open, FileAccess.Read);

            StreamReader reader = new StreamReader(fs);
            string empstring;
            while ((empstring = reader.ReadLine()) != null)
            {
                string[] empDetails = empstring.Split(',');
                Employee emp = new Employee();
                emp.EmpNo = int.Parse(empDetails[0]);
                emp.Name = empDetails[1];
                emp.Designation = empDetails[2];
                emp.Salary = double.Parse(empDetails[3]);
                emp.Commission = double.Parse(empDetails[4]);
                emp.DeptNo = int.Parse(empDetails[5]);
                EmpList.Add(emp.EmpNo, emp);
            }

            reader.Close();
            fs.Close();
        }

        static void getDeptDetails()
        {
            FileStream fs = new FileStream(@"C:\CDAC\MS.NET\shweta_36163_ms.net\Assignment\Assignment6\dept.csv", FileMode.Open, FileAccess.Read);

            StreamReader reader = new StreamReader(fs);
            string deptstring;
            while ((deptstring = reader.ReadLine()) != null)
            {
                string[] deptDetails = deptstring.Split(',');
                Department dept = new Department();
                dept.DeptNo = int.Parse(deptDetails[0]);
                dept.DeptName = deptDetails[1];
                dept.Location = deptDetails[2];
                DeptList.Add(dept.DeptNo, dept);
            }

            reader.Close();
            fs.Close();
        }
        #endregion

        #region Find Employee with no Departments
        private static List<string> FindDeptsWithNoEmps()
        {
            List<string> deptName = new List<string>();
            foreach (int deptkey in DeptList.Keys)
            {
                int count = 0;
                foreach (int key in EmpList.Keys)
                {
                    if (EmpList[key].DeptNo == deptkey)
                    {
                        count++;
                    }
                }
                if (count == 0)
                {
                    deptName.Add(DeptList[deptkey].DeptName);
                }
            }
            return deptName;
        }
        #endregion

        #region Calculate Total Salary
        private static Dictionary<int, double> Calculate_Total_Salary()
        {
            Dictionary<int, double> empSalary = new Dictionary<int, double>();
            foreach (int empkey in EmpList.Keys)
            {
                double netSalary = Convert.ToDouble(EmpList[empkey].Salary) + Convert.ToDouble(EmpList[empkey].Commission);
                empSalary.Add(empkey, netSalary);
            }
            return empSalary;
        }
        #endregion

        #region Get all Employees by Department
        private static void GetAllEmployeesByDept(int deptNo)
        {
            foreach (int employeekey in EmpList.Keys)
            {
                if (EmpList[employeekey].DeptNo == deptNo)
                {
                    Console.WriteLine(EmpList[employeekey]);
                }
            }
        }
        #endregion

        #region Department wise Count
        private static Dictionary<int, int> DeptwiseStaffCount()
        {
            Dictionary<int, int> countStaff = new Dictionary<int, int>();
            foreach (int deptkey in DeptList.Keys)
            {
                int count = 0;
                foreach (int empkey in EmpList.Keys)
                {
                    if (deptkey == EmpList[empkey].DeptNo)
                    {
                        count++;
                    }
                }
                countStaff.Add(deptkey, count);
            }
            return countStaff;
        }
        #endregion

        #region department wise Avarage Salary
        private static Dictionary<int, double> DeptwiseAvgSal()
        {
            Dictionary<int, double> avgSal = new Dictionary<int, double>();
            foreach (int deptkey in DeptList.Keys)
            {
                double totalsalary = 0;
                int count = 0;
                foreach (int empkey in EmpList.Keys)
                {
                    if (deptkey == EmpList[empkey].DeptNo)
                    {
                        totalsalary += EmpList[empkey].Salary;
                        count++;
                    }
                }
                avgSal.Add(deptkey, totalsalary / count);
            }
            return avgSal;
        }
        #endregion

        #region Department wise Minimum salary 
        private static Dictionary<int, double> DeptwiseMinSal()
        {
            Dictionary<int, double> minSal = new Dictionary<int, double>();
            foreach (int deptkey in DeptList.Keys)
            {
                double minsalary = double.MaxValue;
                foreach (int empkey in EmpList.Keys)
                {
                    if (deptkey == EmpList[empkey].DeptNo)
                    {
                        if (EmpList[empkey].Salary < minsalary)
                        {
                            minsalary = EmpList[empkey].Salary;
                        }
                    }
                }

                if (minsalary == double.MaxValue)
                {
                    minsalary = 0;
                }
                minSal.Add(deptkey, minsalary);
            }
            return minSal;
        } 
        #endregion
    }
}
