﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace structBook
{
    struct Book
    {
        private string title; 
        private bool outofstock; 
        private string author;
        private int isbn; 
        private char index; 
        private double price;

        public Book(string title, bool outofstock, string author, int isbn, char index, double price)
        {
            this.title = title;
            this.outofstock = outofstock;
            this.author = author;
            this.isbn = isbn;
            this.index = index;
            this.price = price;
        }

        public void setTitle(string title)
        {
            this.title = title;
        }

        public void setOutofstock(bool outofstock)
        {
            this.outofstock = outofstock;
        }

        public void setAuthor(string author)
        {
            this.author = author;
        }

        public void setIsbn(int isbn)
        {
            this.isbn = isbn;
        }

        public void setIndex(char index)
        {
            this.index = index;
        }

        public void setPrice(double price)
        {
            this.price = price;
        }

        public string getTitle()
        {
            return this.title;
        }

        public bool getOutofstock()
        {
            return this.outofstock;
        }

        public string getAuthor()
        {
            return this.author;
        }

        public int getIsbn()
        {
            return this.isbn;
        }

        public char getIndex()
        {
            return this.index;
        }

        public double getPrice()
        {
            return this.price;
        }

        public void AcceptDetails()
        {
            Console.WriteLine("Enter the Title");
            this.title = Console.ReadLine();
            Console.WriteLine("Enter the outofstock value");
            this.outofstock = Convert.ToBoolean(Console.ReadLine());
            Console.WriteLine("Enter the author");
            this.author = Console.ReadLine();
            Console.WriteLine("Enter the isbn value");
            this.isbn = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the index");
            this.index = Convert.ToChar(Console.ReadLine());
            Console.WriteLine("Enter the price");
            this.price = Convert.ToDouble(Console.ReadLine());
        }

        public void PrintDetails()
        {
            Console.WriteLine("Title  : " + this.title);
            Console.WriteLine("Out of Stock : " + this.outofstock);
            Console.WriteLine("Author : " + this.author);
            Console.WriteLine("isbn : " + this.isbn);
            Console.WriteLine("index : " + this.index);
            Console.WriteLine("price : " + this.price);
            Console.ReadLine();
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Book b1 = new Book();
            b1.AcceptDetails();
            b1.PrintDetails();
        }
    }
}
