﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLoggerLib
{
    public class Singleton
    {
        private static Singleton logger = new Singleton();
        private Singleton()
        {
            Console.WriteLine("Logger Object created");
        }

        public static Singleton CurrentSingleton
        {
            get
            {
                return logger;
            }
        }

        public void Logging(string msg)
        {
            Console.WriteLine("Logged " + msg + " @ " +DateTime.Now.ToString() );
        }
    }
}
