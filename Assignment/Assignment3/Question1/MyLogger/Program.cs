﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyLoggerLib;

namespace MyLogger
{
    class Program
    {
        static void Main(string[] args)
        {
            Singleton.CurrentSingleton.Logging("Singleton  getting called....");

            Console.ReadLine();
        }
    }
}
