﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        public static void Main(String[] args)
        {
            Optional_AssignmentEntities opObj = new Optional_AssignmentEntities();
            //take input
            #region Dataset
            //DataSet ds = new DataSet();

            //DataTable table1 = new DataTable("city-pin-states");
            //DataColumn col1 = new DataColumn("pin_code", typeof(int));
            //DataColumn col2 = new DataColumn("city", typeof(string));
            //DataColumn col3 = new DataColumn("state", typeof(string));

            //table1.PrimaryKey = new DataColumn[] { col1 };
            //table1.Columns.Add(col1);
            //table1.Columns.Add(col2);
            //table1.Columns.Add(col3);

            //string isContinue = "y";
            //do
            //{
            //    DataRow row = table1.NewRow();
            //    Console.WriteLine("Enter the PinCode : ");
            //    row["pin_code"] = Convert.ToInt32(Console.ReadLine());
            //    Console.WriteLine("Enter City Name : ");
            //    row["city"] = Console.ReadLine();
            //    Console.WriteLine("Enter State Name : ");
            //    row["state"] = Console.ReadLine();

            //    table1.Rows.Add(row);
            //    Console.WriteLine("Do you want to continue? y/n");
            //    isContinue = Console.ReadLine();

            //} while (isContinue=="y"); 
            #endregion

            //output
            var result1 = statesHavingSingleCity(opObj.city_pin_states.ToList());
            foreach (var item in result1)
            {
                Console.WriteLine(item);
            }
 
            var result2 = customerOfMaxOrderTotal(opObj.customers, opObj.orders);
            Console.WriteLine(result2);
        }

        private static string customerOfMaxOrderTotal(DbSet<customer> customers, DbSet<order> orders)
        {
            var CustId = (from order in orders
                          group order by order.customer_id into orderGroup
                          orderby orderGroup.Sum(o => o.amount) descending
                          select orderGroup.Key).First();

            int Id = Convert.ToInt32(CustId);

            var Result = (from cust in customers
                          where cust.Customer_Id == Id
                          select cust.Name).First();
            return Result;
        }

        private static List<string> statesHavingSingleCity(List<city_pin_states> pinStatesList)
        {
            var result = (from pinState in pinStatesList
                             group pinState by pinState.state into stategroup
                             where stategroup.Count()==1
                          select stategroup.Key).ToList();
                                                 
            return result;
        }
    }
}
