﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace CRUD_Operation
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;

            #region Read operation
            //try
            //{
            //    conn = new SqlConnection(@"Server=(LocalDB)\MSSQLLocalDB;database=sunbeamDB;Integrated Security=true");
            //    conn.Open();
            //    cmd = new SqlCommand("Select * from Emp",conn);

            //    SqlDataReader reader = cmd.ExecuteReader();

            //    while (reader.Read())
            //    {
            //        Console.WriteLine(String.Format("{0}---{1}---{2}",reader["Id"],reader["Name"],reader["Address"]));
            //    }
            //}
            //catch(Exception ex)
            //{
            //    Console.WriteLine("Something is not right here . try again!!");
            //    Console.WriteLine("Technical Details : "+ex.Message);
            //}
            //finally
            //{
            //    conn.Close();
            //}
            //Console.ReadLine(); 
            #endregion

            #region insert Data 
            //try
            //{
            //    conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;database=sunbeamDB;Integrated Security=true");
            //    conn.Open();

            //    Console.WriteLine("Enter the Id : ");
            //    int id = Convert.ToInt32(Console.ReadLine());
            //    Console.WriteLine("Enter the Name : ");
            //    string name = Console.ReadLine();
            //    Console.WriteLine("Enter the Address : ");
            //    string address = Console.ReadLine();

            //    string insertQuery = "insert into Emp values ({0},'{1}','{2}')";
            //    string valueQuery = String.Format(insertQuery, id, name, address);

            //    cmd = new SqlCommand(valueQuery, conn);

            //    int NoofColumnAffected = cmd.ExecuteNonQuery();

            //    Console.WriteLine("No of Column Affected : "+NoofColumnAffected);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine("Something is not right here . try again!!");
            //    Console.WriteLine("Technical Details : " + ex.Message);
            //}
            //finally
            //{
            //    conn.Close();
            //}
            //Console.ReadLine(); 
            #endregion

            #region update Data
            //try
            //{
            //    conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;database=sunbeamDB;Integrated Security=true");
            //    conn.Open();

            //    Console.WriteLine("Enter the Id : ");
            //    int id = Convert.ToInt32(Console.ReadLine());
            //    Console.WriteLine("Enter the Name : ");
            //    string name = Console.ReadLine();
            //    Console.WriteLine("Enter the Address : ");
            //    string address = Console.ReadLine();

            //    string insertQuery = "update Emp set Name='{1}',Address='{2}' where Id={0}";
            //    string valueQuery = String.Format(insertQuery, id, name, address);

            //    cmd = new SqlCommand(valueQuery, conn);

            //    int NoofColumnAffected = cmd.ExecuteNonQuery();

            //    Console.WriteLine("No of Column Affected : " + NoofColumnAffected);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine("Something is not right here . try again!!");
            //    Console.WriteLine("Technical Details : " + ex.Message);
            //}
            //finally
            //{
            //    conn.Close();
            //}
            //Console.ReadLine(); 
            #endregion

            #region Delete operation
            try
            {
                conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;database=sunbeamDB;Integrated Security=true");
                conn.Open();

                Console.WriteLine("Enter the Id : ");
                int id = Convert.ToInt32(Console.ReadLine());


                string insertQuery = "delete from Emp where Id={0}";
                string valueQuery = String.Format(insertQuery, id);

                cmd = new SqlCommand(valueQuery, conn);

                int NoofColumnAffected = cmd.ExecuteNonQuery();

                Console.WriteLine("No of Column Affected : " + NoofColumnAffected);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something is not right here . try again!!");
                Console.WriteLine("Technical Details : " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            Console.ReadLine(); 
            #endregion
        }
    }
}
