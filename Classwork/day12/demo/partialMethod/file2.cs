﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace partialMethod
{
    public partial class Emp
    {
        partial void Validate(string propertyName, object value)
        {
            if (propertyName == "Age")
            {
                int age = Convert.ToInt32(value);
                if(age<18 || age > 60)
                {
                    Console.WriteLine("Age is set up Wrong.......!!!!");
                }
            }
        }

        public string GetDetails()
        {
            return "Details of Emp Class...";
        }
    }
}
