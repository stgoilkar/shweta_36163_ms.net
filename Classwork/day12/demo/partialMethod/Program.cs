﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace partialMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Partial Method
            //Emp emp = new Emp();
            //emp.Age = 1; 
            #endregion

            #region Dynamic Type

            //factory fact = new factory();
            //Console.WriteLine("1 : Employee , 2 : Book ");
            //Console.WriteLine("Enter the Choice.... ");

            //int choice = Convert.ToInt32(Console.ReadLine());
            //dynamic obj = fact.getMeObject(choice);

            //Console.WriteLine(obj.GetDetails()); 
            #endregion

            #region Optional Parameter
            //Player player = new Player();
            //Console.WriteLine(player.GetDetails(1,"Sachin","Mumbai"));

            //Player player1 = new Player();
            //Console.WriteLine(player1.GetDetail(2,"Virat")); 
            #endregion

            #region  Named Parameter
            Player player = new Player();
            Console.WriteLine(player.GetDetail(3, address: "Pune"));

            Console.WriteLine(player.GetDetail(4, Name: "Malinga")); 
            #endregion
        }
    }

    public class Book
    {
        public string GetDetails()
        {
            return "Details of Book Class...";
        }
    }

    public class factory
    {
        public object getMeObject(int choice)
        {
            if (choice == 1)
            {
                return new Emp();
            }
            else
            {
                return new Book();
            }
        }
    }

    public class Player
    {
        public string GetDetails(int no,string Name,string address)
        {
            return string.Format("Details are : No is {0}, your Name is {1} and Address is {2}", no, Name, address);
        }

        public string GetDetail(int no, string Name="MS Dhoni", string address="Delhi")
        {
            return string.Format("Details are : No is {0}, your Name is {1} and Address is {2}", no, Name, address);
        }
    }
}
