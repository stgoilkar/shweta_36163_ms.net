﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getter_setter
{
    class Program
    {
        static void Main(string[] args)
        {
            //Person person = new Person();
            //person.Set_Name("Shweta");
            //person.Set_Age(24);
            //Console.WriteLine(person.Get_Name());
            //Console.WriteLine(person.Get_Age());

            Person person = new Person("Shweta", 24);
            Console.WriteLine(person.GetDetails());

            Console.ReadLine();
        }
    }

    public class Person
    {
        #region Private Member
        private string _Name;
        private int _Age;
        #endregion

        #region Constructor
        public Person()
        {
            this._Name = "";
            this._Age = 0;
        }
        #endregion

        #region paramerdized Constructor
        public Person(string name,int age)
        {
            this._Name = name;
            this._Age = age;
        }
        #endregion

        #region Getter setter
        /// <summary>
        ///  personObject.Set_Name("shweta") -- this is used to set the name
        /// </summary>
        /// <param name="name">This is the Name of the Person</param>
        public void Set_Name(string name)
        {
            if(name != "")
            {
                this._Name = name;
            }
            else
            {
                this._Name = "no data";
            }
        }

        public string Get_Name()
        {
            return "Mr /Mrs " + this._Name;
        }

        /// <summary>
        /// This is used to set the age
        /// </summary>
        /// <param name="age"> this is the Age of the Person</param>
        public void Set_Age(int age)
        {
            this._Age = age;
        }
        public int Get_Age()
        {
            return this._Age;
        }

        #endregion

        #region Methods
        public string GetDetails()
        {
            return "Welcome " + this.Get_Name() + " Your Age is " + this.Get_Age().ToString();
        }
        #endregion
    }
}
