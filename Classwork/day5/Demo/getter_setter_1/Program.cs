﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getter_setter_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person();
            person.Name = "Shweta";
            person.Age = 24;
            Console.WriteLine(person.GetDetails());

            Console.ReadLine();
        }
    }

    public class Person
    {
        #region Private Member
        private string _Name;
        private int _Age;
        #endregion

        #region Constructor
        public Person()
        {
            this._Name = "";
            this._Age = 0;
        }
        #endregion

        #region paramerdized Constructor
        public Person(string name, int age)
        {
            this._Name = name;
            this._Age = age;
        }
        #endregion

        #region Getter setter
        /// <summary>
        ///  personObject.Set_Name("shweta") -- this is used to set the name
        /// </summary>
        /// <param name="name">This is the Name of the Person</param>
        
        public string Name
        {
            get
            {

                return "Mr /Mrs " + this._Name;
            }
            set
            {
                if (value != "")
                {
                    this._Name = value;
                }
                else
                {
                    this._Name = "no data";
                }
            }
        }

        /// <summary>
        /// This is used to set the age
        /// </summary>
        /// <param name="age"> this is the Age of the Person</param>
        /// 

        public int Age
        {
            set
            {
                this._Age = value;
            }
            get
            {
                return this._Age;
            }
        }
        #endregion

        #region Methods
        public string GetDetails()
        {
            return "Welcome " + this._Name + " Your Age is " + this._Age;
        }
        #endregion
    }
}

