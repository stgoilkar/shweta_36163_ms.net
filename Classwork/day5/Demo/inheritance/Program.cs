﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee emp = new Employee();
            emp.Name = "Shweta";
            emp.Age = 24;
            emp.DName = "Electronics";
            Console.WriteLine(emp.GetDetails());

            ConsultantEmployee cemp = new ConsultantEmployee();
            cemp.Name = "Lalita";
            cemp.Age = 43;
            cemp.DName = "IT";
            cemp.workingHours = 8;
            Console.WriteLine(cemp.GetDetails());

            Console.ReadLine();
        }
    }

    public class Person
    {
        #region Private Member
        private string _Name;
        private int _Age;
        #endregion

        #region Constructor
        public Person()
        {
            this._Name = "";
            this._Age = 0;
        }
        #endregion

        #region paramerdized Constructor
        public Person(string name, int age)
        {
            this._Name = name;
            this._Age = age;
        }
        #endregion

        #region Getter setter
        /// <summary>
        ///  personObject.Set_Name("shweta") -- this is used to set the name
        /// </summary>
        /// <param name="name">This is the Name of the Person</param>

        public string Name
        {
            get
            {

                return "Mr /Mrs " + this._Name;
            }
            set
            {
                if (value != "")
                {
                    this._Name = value;
                }
                else
                {
                    this._Name = "no data";
                }
            }
        }

        /// <summary>
        /// This is used to set the age
        /// </summary>
        /// <param name="age"> this is the Age of the Person</param>
        /// 

        public int Age
        {
            set
            {
                this._Age = value;
            }
            get
            {
                return this._Age;
            }
        }
        #endregion

        #region Methods
        public virtual string GetDetails()
        {
            return "Welcome " + this._Name + " Your Age is " + this._Age;
        }
        #endregion
    }

    public class Employee:Person
    {
        private string _DName;                      

        public string DName
        {
            get { return _DName; }
            set { _DName = value; }
        }

        public override string GetDetails()
        {
            return base.GetDetails() + " from Department " + this.DName;
        }
    }

    public class ConsultantEmployee : Employee
    {
        private int _workingHours;

        public int workingHours
        {
            get { return _workingHours; }
            set { _workingHours = value; }
        }

        public override string GetDetails()
        {
            return base.GetDetails() + " Working Hours are " + this.workingHours;
        }
    }
}

