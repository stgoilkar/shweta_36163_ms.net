﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace overloading
{
    class Program
    {
        static void Main(string[] args)
        {
            Maths m1 = new Maths();
            Console.WriteLine(m1.add(10,20));
            Console.WriteLine(m1.add(50,20,30));

            Console.ReadLine();
        }
    }

    public class Maths
    {
        public int add(int x,int y)
        {
            return x + y;
        }

        public int add(int x, int y,int z)
        {
            return x + y +z;
        }
    }
}
