﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logger
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Logger.CurrentLogger.Logging("You are logged in");

                emp e = new emp();
                e.GetDetails();

                Maths m1 = new Maths();

                Console.WriteLine("Give the value of x");
                int x =Convert.ToInt32( Console.ReadLine());

                Console.WriteLine("Give the value of y");
                int y = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(m1.add(x, y));
            }
            catch (Exception ex)
            {
                Logger.CurrentLogger.Logging("Exception Occur....." + ex.Message);
            }
            
            Console.ReadLine();
        }
    }

    public class emp
    {
        public void GetDetails()
        {
            Logger.CurrentLogger.Logging("Emp GetDetails.....");
        }
    }

    public class Maths
    {
        public int add(int x,int y)
        {
            Logger.CurrentLogger.Logging("Addition of " + x.ToString() + " and " + y.ToString());
            return x + y;
        }
    }

    public class Logger
    {
        private static Logger logger = new Logger();

        private Logger()
        {
            Console.WriteLine("Logger Constructor");
        }

        public static Logger CurrentLogger
        {
            get
            {
                return logger;
            }
        }

        public void Logging(string msg)
        {
            Console.WriteLine("Logged : " + msg + " @ " + DateTime.Now.ToString());
        }
    }
}
