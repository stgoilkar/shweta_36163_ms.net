﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace TestThreads
{
    class Program
    {
        static void Main(string[] args)
        {

            Stopwatch watch = new Stopwatch();
            watch.Start();



            for (int i = 0; i < 10; i++)
            {
                Thread t = new Thread(DoSomeThingVeryComplex);
                t.Start();

                //Task t = new Task(DoSomeThingVeryComplex);
                //t.Start();
                DoSomeThingVeryComplex();
            }

           
            watch.Stop();
            Console.WriteLine("Total Time taken = {0}", watch.ElapsedMilliseconds);
            Console.ReadLine();
        }

        static void DoSomeThingVeryComplex()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();


            for (int i = 0; i < 100000; i++)
            {
                for (int j = 0; j < 1000; j++)
                {

                }
            }
            watch.Stop();
            Console.WriteLine("Time taken by Thread No{1} = {0}", watch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId);

            Console.WriteLine("Thread No Executing Code {0}", Thread.CurrentThread.ManagedThreadId);
        }
    }
}
