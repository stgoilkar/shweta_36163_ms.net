﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Consumer2.proxy;

namespace Consumer2
{
    class Program
    {
        static void Main(string[] args)
        {
            DatabaseServicesSoapClient obj = new DatabaseServicesSoapClient();
            Console.WriteLine( obj.Add(20, 30));

            Console.WriteLine(obj.Sub(100,1));
        }
    }
}
