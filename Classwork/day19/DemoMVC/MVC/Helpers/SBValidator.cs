﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Helpers
{
    public class SBValidator:ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            bool IsValid=false;
            if(value != null)
            {
                if (value.ToString()=="1234")
                {
                    IsValid = false;
                }
                else
                {
                    IsValid = true;
                }
            }
            return IsValid;
        }
    }
}