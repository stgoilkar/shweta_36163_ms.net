﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class Contact
    {
        public string Name { get; set; }
        public string Number { get; set; }
        public string Email { get; set; }
        public string  Query { get; set; }

        public override string ToString()
        {
            return String.Format("Hello Your Details...... Name : '{0}' , Your Number is '{1}' and Email Address is '{2}' , Your Query is '{3}'", Name, Number, Email, Query);
        }
    }
}