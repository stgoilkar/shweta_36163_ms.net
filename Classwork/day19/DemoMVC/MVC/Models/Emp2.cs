﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MVC.Helpers;

namespace MVC.Models
{
    [MetadataType(typeof(Extra_info))]
    public partial class Emp
    {
    }

    public class Extra_info
    {
        [Required(ErrorMessage ="Id cant be null")]
        [Range(1,100,ErrorMessage ="Enter the value between 1 to 100")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name cant be null")]
        [SBValidator(ErrorMessage ="Enter valid Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Address cant be null")]
        public string Address { get; set; }
    }
}