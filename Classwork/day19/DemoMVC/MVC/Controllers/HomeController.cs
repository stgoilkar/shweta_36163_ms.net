﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Net.Mail;
using MVC.Models;
using System.Net;

namespace MVC.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Username = User.Identity.Name;
            ViewData["Msg"] = "Welcome to HR App..";
            var allEmp=dbObj.Emps.ToList();
            return View(allEmp);
        }

        [HttpGet]
        public ActionResult Edit(int Id)
        {
            ViewBag.Username = User.Identity.Name;
            ViewBag.msg= "Edit Data";
            Emp empToBeUpdated = (from Emp in dbObj.Emps.ToList()
                                  where Emp.Id == Id
                                  select Emp).First();
            return View(empToBeUpdated);
        }

        [HttpPost]
        public ActionResult Edit(Emp empUpdate)
        {
            try
            {
                ViewBag.Username = User.Identity.Name;
                Emp empToBeUpdated = (from Emp in dbObj.Emps.ToList()
                                      where Emp.Id == empUpdate.Id
                                      select Emp).First();

                empToBeUpdated.Name = empUpdate.Name;
                empToBeUpdated.Address = empUpdate.Address;

                dbObj.SaveChanges();
                return Redirect("/Home/Index");
            }
            catch (Exception ex)
            {
                return View("Error", ex);
            }
        }

        public ActionResult Delete(int Id)
        {
            ViewBag.Username = User.Identity.Name;
            Emp empToBeDeleted = (from Emp in dbObj.Emps.ToList()
                                  where Emp.Id ==Id
                                  select Emp).First();


            dbObj.Emps.Remove(empToBeDeleted);
            dbObj.SaveChanges();
            return Redirect("/Home/Index");
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Username = User.Identity.Name;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Emp emp)
        {
            ViewBag.Username = User.Identity.Name;
            dbObj.Emps.Add(emp);
            dbObj.SaveChanges();

            return Redirect("/Home/Index");
        }

        [AllowAnonymous]

        public ActionResult About()
        {
            ViewBag.Username = User.Identity.Name;
            return View();
        }
        [AllowAnonymous]
        public ActionResult Contact()
        {
            ViewBag.Username = User.Identity.Name;
            ViewBag.Method = "POST";
            ViewBag.Action = "/Home/Contact";
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Contact(Contact contactObj)
        {
            try
            {
                ViewBag.Username = User.Identity.Name;
                string emailUserName = ConfigurationManager.AppSettings["email"];
                string emailUserPassword = ConfigurationManager.AppSettings["password"];

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(emailUserName);
                mail.To.Add(contactObj.Email);
                mail.CC.Add("stgoilkar@gmail.com");

                mail.Subject = "New Query Received...!!!";
                mail.Body = "<h4>" + contactObj.ToString() + "</h4>";

                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);

                smtp.Credentials = new NetworkCredential(emailUserName, emailUserPassword);

                smtp.EnableSsl = true;

                smtp.Send(mail);
                ViewBag.message = "Your Query submitted Successfully....";

                return View();

            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return View();
            }    
        }
    }
}