﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;
using System.Web.Security;

namespace MVC.Controllers
{
    public class LoginController : Controller
    {
        sunbeamDBEntities dbObj = new sunbeamDBEntities();
        // GET: Login
        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(HrLoginInfo loginInfo,string ReturnUrl)
        {
            var result =( from hr in dbObj.HrLoginInfoes.ToList()
                          where hr.UserName.ToLower() == loginInfo.UserName.ToLower()
                          && hr.Password == loginInfo.Password
                          select hr).ToList().Count();

            if (result == 1) 
            {
                FormsAuthentication.SetAuthCookie(loginInfo.UserName, false);
                if (ReturnUrl == null)
                {
                    return Redirect("/Home/Index");
                }
                else
                {
                    return Redirect(ReturnUrl);
                }
            }
            else
            {
                ViewBag.ErrorMessage = "Enter Valid Credentials...";
                return View();
            }
        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Login/SignIn");
        }
    }
}