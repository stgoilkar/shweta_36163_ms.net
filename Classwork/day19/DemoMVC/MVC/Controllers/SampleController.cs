﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;

namespace MVC.Controllers
{
    public class SampleController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.Username = User.Identity.Name;
            ViewData["Msg"] = "Welcome to HR App..";
            var allEmp = dbObj.Emps.ToList();
            return View(allEmp);
        }

        [HttpGet]
        public ActionResult Edit(int Id)
        {
            ViewBag.Username = User.Identity.Name;
            ViewBag.msg = "Edit Data";
            Emp empToBeUpdated = (from Emp in dbObj.Emps.ToList()
                                  where Emp.Id == Id
                                  select Emp).First();
            return View(empToBeUpdated);
        }

        [HttpPost]
        public ActionResult Edit(Emp empUpdate)
        {
            try
            {
                ViewBag.Username = User.Identity.Name;
                Emp empToBeUpdated = (from Emp in dbObj.Emps.ToList()
                                      where Emp.Id == empUpdate.Id
                                      select Emp).First();

                empToBeUpdated.Name = empUpdate.Name;
                empToBeUpdated.Address = empUpdate.Address;

                dbObj.SaveChanges();
                return Redirect("/Sample/Index");
            }
            catch (Exception ex)
            {
                return View("Error", ex);
            }
        }

        public ActionResult Delete(int Id)
        {
            ViewBag.Username = User.Identity.Name;
            Emp empToBeDeleted = (from Emp in dbObj.Emps.ToList()
                                  where Emp.Id == Id
                                  select Emp).First();


            dbObj.Emps.Remove(empToBeDeleted);
            dbObj.SaveChanges();
            return Redirect("/Sample/Index");
        }

        public JsonResult Validate(int Id)
        {
            int count = (from emp in dbObj.Emps.ToList()
                         where emp.Id == Id
                         select emp).Count();
            return new JsonResult() { Data=count,JsonRequestBehavior=JsonRequestBehavior.AllowGet};
        }
        
        [HttpGet]
        public ActionResult Create()
        {

            ViewBag.Username = User.Identity.Name;
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(Emp emp)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Username = User.Identity.Name;
                dbObj.Emps.Add(emp);
                dbObj.SaveChanges();

                return Redirect("/Sample/Index");
            }
            else
            {
                return View(emp);
            }
        }
    }
}