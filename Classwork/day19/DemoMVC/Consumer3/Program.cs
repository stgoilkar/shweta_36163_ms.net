﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Consumer3.proxy;

namespace Consumer3
{
    class Program
    {
        static void Main(string[] args)
        {
            Service1Client obj = new Service1Client();
            foreach (var item in obj.GetEmps())
            {
                Console.WriteLine(item.Name);
            }
        }
    }
}
