﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace xml
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Xml Serialization
            //Emp emp = new Emp();
            //Console.WriteLine("Enter the Number ");
            //emp.No = Convert.ToInt32(Console.ReadLine());
            //Console.WriteLine("Enter the Name ");
            //emp.Name = Console.ReadLine();

            //FileStream fs = new FileStream(@"C:\CDAC\MS.NET\shweta_36163_ms.net\Classwork\day8\data.xml", FileMode.OpenOrCreate, FileAccess.Write);

            //XmlSerializer writer = new XmlSerializer(typeof(Emp));
            //writer.Serialize(fs, emp);

            //writer = null;
            //fs.Flush();
            //fs.Close();

            #endregion

            #region Xml Deserialization

            FileStream fs = new FileStream(@"C:\CDAC\MS.NET\shweta_36163_ms.net\Classwork\day8\data.xml", FileMode.Open, FileAccess.Read);

            XmlSerializer reader = new XmlSerializer(typeof(Emp));
            object obj=reader.Deserialize(fs);

            if(obj is Emp)
            {
                Emp e = (Emp)obj;
                Console.WriteLine(e.GetDetails());
            }
            else
            {
                Console.WriteLine("Unknown Data Type...");
            }

            Console.ReadLine();

            reader = null;
            fs.Flush();
            fs.Close();

            #endregion
        }
    }

    public class Emp
    {
        private string _Password = "shweta@123";
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string GetDetails()
        {
            return "No : " + this.No + " , Name : " + this.Name;
        }

    }
}
