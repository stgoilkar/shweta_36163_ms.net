﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;

namespace binary1
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Binary Serialization

            //ArrayList arr = new ArrayList();
            //Emp emp = new Emp();

            //Console.WriteLine("Enter No:");
            //emp.No = Convert.ToInt32(Console.ReadLine());

            //Console.WriteLine("Enter Name");
            //emp.Name = Console.ReadLine();

            //arr.Add(emp);

            //Book b = new Book();

            //Console.WriteLine("Enter ISBN:");
            //b.ISBN = Convert.ToInt32(Console.ReadLine());

            //Console.WriteLine("Enter Title");
            //b.Title = Console.ReadLine();
            //arr.Add(b);

            //FileStream fs = new FileStream(@"C:\CDAC\MS.NET\shweta_36163_ms.net\Classwork\day8\data1.txt", FileMode.OpenOrCreate, FileAccess.Write);
            //BinaryFormatter writer = new BinaryFormatter();

            //writer.Serialize(fs, arr);

            //writer = null;
            //fs.Flush();
            //fs.Close();

            #endregion

            #region Deserialization

            FileStream fs = new FileStream(@"C:\CDAC\MS.NET\shweta_36163_ms.net\Classwork\day8\data1.txt", FileMode.Open, FileAccess.Read);
            BinaryFormatter reader = new BinaryFormatter();

            object obj= reader.Deserialize(fs);

            if(obj is ArrayList)
            {
                ArrayList arr = (ArrayList)obj;
                foreach (object arrayList in arr)
                {
                    if (arrayList is Emp)
                    {
                        Emp e = (Emp)arrayList;
                        Console.WriteLine(e.GetDetails());
                    }
                    else if (arrayList is Book)
                    {
                        Book b = (Book)arrayList;
                        Console.WriteLine(b.GetDetails());
                    }
                }

            }
            else
            {
                Console.WriteLine("Unknown Data Type...");
            }

            Console.ReadLine();

            reader = null;
            fs.Flush();
            fs.Close();
            #endregion

        }
    }

    [Serializable]
    public class Emp
    {
        [NonSerialized]
        private string _Password = "shweta@123";
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string GetDetails()
        {
            return "No : " + this.No + " , Name : " + this.Name;
        }

    }

    [Serializable]
    public class Book
    {
        private int _ISBN;
        private string _Title;

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }


        public int ISBN
        {
            get { return _ISBN; }
            set { _ISBN = value; }
        }

        public string GetDetails()
        {
            return "ISBN : " + this.ISBN + " , Title : " + this.Title;
        }

    }
}
