﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Soap;

namespace SOAP
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Soap Serialization
            //Emp emp = new Emp();
            //Console.WriteLine("Enter the Number ");
            //emp.No = Convert.ToInt32(Console.ReadLine());
            //Console.WriteLine("Enter the Name ");
            //emp.Name = Console.ReadLine();

            //FileStream fs = new FileStream(@"C:\CDAC\MS.NET\shweta_36163_ms.net\Classwork\day8\soap.txt", FileMode.OpenOrCreate, FileAccess.Write);

            //SoapFormatter writer = new SoapFormatter();
            //writer.Serialize(fs, emp);

            //writer = null;
            //fs.Flush();
            //fs.Close();

            #endregion

            #region Soap Deserialization
            FileStream fs = new FileStream(@"C:\CDAC\MS.NET\shweta_36163_ms.net\Classwork\day8\soap.txt", FileMode.Open, FileAccess.Read);

            SoapFormatter reader = new SoapFormatter();
            object obj = reader.Deserialize(fs);

            if (obj is Emp)
            {
                Emp emp = (Emp)obj;
                Console.WriteLine(emp.GetDEtails());
            }
            else
            {
                Console.WriteLine("Unknown Type Data");
            }

            Console.ReadLine();
            reader = null;
            fs.Flush();
            fs.Close();
            #endregion
        }
    }

    [Serializable]
    public class Emp
    {
        [NonSerialized]
        private string _Password = "shweta@123";
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string GetDEtails()
        {
            return "No : " + this.No + " , Name : " + this.Name;
        }

    }
}
