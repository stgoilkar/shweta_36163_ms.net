﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Customer1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Assembly Path : (EXE/DLL)");
            string PathOfAssembly = Console.ReadLine();

            //C:\CDAC\MS.NET\shweta_36163_ms.net\Assignment\Assignment4\Assignment_4\HRMSLib\bin\Debug\HRMSLib.dll

            Assembly assembly = Assembly.LoadFrom(PathOfAssembly);

            Type[] types = assembly.GetTypes();
            foreach(Type type in types)
            {
                Console.WriteLine("Type : " +type.FullName);
                MethodInfo[] methodInfos = type.GetMethods();
                foreach (MethodInfo method in methodInfos)
                {
                    Console.WriteLine(method.ReturnType +" " +method.Name + " ( ");

                    ParameterInfo[] parameterInfos = method.GetParameters();

                    foreach (ParameterInfo parameter in parameterInfos)
                    {
                        Console.WriteLine(parameter.ParameterType.ToString() + " " +parameter.Name + " ");
                    }
                    Console.WriteLine(")");
                    Console.WriteLine();
                }
            }
            
          
            Console.ReadLine();
        }
    }
}
