﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace SQLInjection
{
    class Program
    {
        static void Main(string[] args)
        {
            string constr = ConfigurationManager.ConnectionStrings["connString"].ToString();

            SqlConnection conn = new SqlConnection(constr);

            Console.WriteLine("Enter the Name : ");
            string name = Console.ReadLine();
            string query="select count(*) from Emp where Name='"+name+"'";
            SqlCommand cmd = new SqlCommand(query, conn);

            conn.Open();
            var result=cmd.ExecuteScalar();
            int res = Convert.ToInt32(result);
            if (res > 0)
            {
                Console.WriteLine("User Found.....");
            }
            else
            {
                Console.WriteLine("Invalid User......");
            }

            string selectquery = "select * from Emp where Name='" + name + "'";
            SqlCommand selectcmd = new SqlCommand(selectquery, conn);
            SqlDataReader reader = selectcmd.ExecuteReader();
            while (reader.Read())
            {
                
                Console.WriteLine(String.Format("{0} | {1}",reader["Name"],reader["Address"]));

            }

            conn.Close();
            Console.ReadLine();
        }
    }
}
