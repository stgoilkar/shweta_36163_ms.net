﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModularCRUD.DAL;
using System.Configuration;

namespace ModularCRUD.DAL
{
    public class dbFactory
    {
        public IDatabase GetDatabase()
        {
            int dbchoice = Convert.ToInt32(ConfigurationManager.AppSettings["dbChoice"]);

            if (dbchoice==1)
            {
                return new sqlServer();
            }
            else
            {
                return new oracle();
            }
        }
    }
}
