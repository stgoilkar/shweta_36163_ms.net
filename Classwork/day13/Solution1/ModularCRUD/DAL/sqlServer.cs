﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModularCRUD.POCO;
using System.Data.SqlClient;
using System.Configuration;

namespace ModularCRUD.DAL
{
    public class sqlServer : IDatabase
    {
        public List<Emp> Select()
        {
            List<Emp> allEmps = new List<Emp>();

            string constr = ConfigurationManager.ConnectionStrings["connString"].ToString();
            SqlConnection conn = new SqlConnection(constr);

            string query = "Select * from Emp";
            SqlCommand cmd = new SqlCommand(query, conn);

            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Emp emp = new Emp();
                {
                    emp.No = Convert.ToInt32(reader["Id"]);
                    emp.Name = reader["Name"].ToString();
                    emp.Address = reader["Address"].ToString();
                }
                allEmps.Add(emp);
            }

            conn.Close();
            return allEmps;
        }

        public int Insert(Emp emp)
        {
            string constr = ConfigurationManager.ConnectionStrings["connString"].ToString();
            SqlConnection conn = new SqlConnection(constr);

            string query = "insert into Emp values({0},'{1}','{2}')";
            string insertQuery = string.Format(query, emp.No, emp.Name, emp.Address);
            SqlCommand cmd = new SqlCommand(insertQuery, conn);

            conn.Open();
            int rowsAffected = cmd.ExecuteNonQuery();

            conn.Close();
            return rowsAffected;
        }

        public int Update(Emp emp)
        {
            string constr = ConfigurationManager.ConnectionStrings["connString"].ToString();
            SqlConnection conn = new SqlConnection(constr);

            string query = "update Emp set Name='{1}', Address='{2}' where Id={0}";
            string updateQuery = string.Format(query, emp.No, emp.Name, emp.Address);
            SqlCommand cmd = new SqlCommand(updateQuery, conn);

            conn.Open();
            int rowsAffected = cmd.ExecuteNonQuery();

            conn.Close();
            return rowsAffected;
        }

        public int Delete(int No)
        {
            string constr = ConfigurationManager.ConnectionStrings["connString"].ToString();
            SqlConnection conn = new SqlConnection(constr);

            string query = "delete From Emp where Id={0}";
            string deleteQuery = string.Format(query, No);
            SqlCommand cmd = new SqlCommand(deleteQuery, conn);

            conn.Open();
            int rowsAffected = cmd.ExecuteNonQuery();

            conn.Close();
            return rowsAffected;
        }



    }
}
