﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DisconnectedArchitecture
{
    class Program
    {
        static void Main(string[] args)
        {
            #region  Fetch Data Using Disconnected Architecture
            DataSet dataSet = new DataSet();

            SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=sunbeamDB;Integrated Security=True;Pooling=False");

            SqlDataAdapter adapter = new SqlDataAdapter("Select * from Emp", conn);
            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            SqlCommandBuilder cmd = new SqlCommandBuilder(adapter);

            adapter.Fill(dataSet, "abc");
            #endregion

            #region Insert using Disconnected Architecture
            //DataRow row = dataSet.Tables["abc"].NewRow();

            //row["Id"] = 7;
            //row["Name"] = "Hanuman";
            //row["Address"] = "Ayodhya";

            //dataSet.Tables[0].Rows.Add(row);
            //adapter.Update(dataSet, "abc"); 
            #endregion

            #region Update using Disconnected Architecture

            //Console.WriteLine("Enter the Id of employee whose data you want to Update");
            //int Id = Convert.ToInt32(Console.ReadLine());

            //DataRow rowModified = dataSet.Tables[0].Rows.Find(Id);
            //if (rowModified != null)
            //{
            //    Console.WriteLine("Employee Found...");
            //    Console.WriteLine("Name : " + rowModified["Name"].ToString());
            //    Console.WriteLine("Address : " + rowModified["Address"].ToString());
            //    Console.WriteLine("--------------------------------------------------------------");
            //    Console.WriteLine("Enter the new Data..");
            //    Console.WriteLine("Enter the Name : ");
            //    rowModified["Name"] = Console.ReadLine();
            //    Console.WriteLine("Enter the Addresa...");
            //    rowModified["Address"] = Console.ReadLine();
            //}
            //else
            //{
            //    Console.WriteLine("Employee Not Found....");
            //}

            //adapter.Update(dataSet, "abc"); 
            #endregion

            #region Deleted using Disconnected Architecture

            Console.WriteLine("Enter the Id of employee whose data you want to Delete");
            int Id = Convert.ToInt32(Console.ReadLine());

            DataRow rowDeleted = dataSet.Tables[0].Rows.Find(Id);
            if (rowDeleted != null)
            {
                Console.WriteLine("Employee Found...");
                rowDeleted.Delete();
                Console.WriteLine("Deleted Successfully...");
            }
            else
            {
                Console.WriteLine("Employee Not Found....");
            }

            adapter.Update(dataSet, "abc");
            #endregion
        }
    }
}
