﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace dataSet1
{
    class Program
    {
        static void Main(string[] args)
        {
            DataSet dataSet = new DataSet();
            DataTable table = new DataTable("Employee");

            DataColumn column1 = new DataColumn("No", typeof(int));
            DataColumn column2 = new DataColumn("Name", typeof(string));
            DataColumn column3 = new DataColumn("Address", typeof(string));

            table.Columns.Add(column1);
            table.Columns.Add(column2);
            table.Columns.Add(column3);

            table.PrimaryKey = new DataColumn[] { column1 };

            SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=sunbeamDB;Integrated Security=True;Pooling=False");

            conn.Open();
            SqlCommand cmd = new SqlCommand("Select * from Emp", conn);

            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                DataRow r = table.NewRow();
                r["No"] = Convert.ToInt32(reader["Id"]);
                r["Name"] = reader["Name"].ToString();
                r["Address"] = reader["Address"].ToString();

                table.Rows.Add(r);
            }

            conn.Close();

            dataSet.Tables.Add(table);

            DataRow completeNewRow = dataSet.Tables[0].NewRow();
            completeNewRow["No"] = 8;
            completeNewRow["Name"] = "Gangu";
            completeNewRow["Address"] = "Satara";

            dataSet.Tables[0].Rows.Add(completeNewRow);

            DataTable table1 = new DataTable("Book");
            DataColumn ISBN = new DataColumn("ISBN", typeof(int));
            DataColumn title = new DataColumn("Title", typeof(string));

            table1.Columns.Add(ISBN);
            table1.Columns.Add(title);

            DataRow row1 = table1.NewRow();
            row1["ISBN"] = 1234;
            row1["title"] = "Secrete";

            table1.Rows.Add(row1);
            dataSet.Tables.Add(table1);
        }
    }
}
