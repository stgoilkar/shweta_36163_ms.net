﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace StoredProcedure
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection conn = null;
            try
            {
                string Constr = ConfigurationManager.ConnectionStrings["connString"].ToString();

                conn = new SqlConnection(Constr);

                SqlCommand cmd = new SqlCommand("sp", conn);

                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter parameter1 = new SqlParameter("@No", SqlDbType.Int);
                Console.WriteLine("Enter the No :   ");
                parameter1.Value = Convert.ToInt32(Console.ReadLine());

                SqlParameter parameter2 = new SqlParameter("@Name", SqlDbType.VarChar, 50);
                Console.WriteLine("Enter the Name : ");
                parameter2.Value = Console.ReadLine();

                SqlParameter parameter3 = new SqlParameter("@Address", SqlDbType.VarChar);
                Console.WriteLine("Enter the Address : ");
                parameter3.Value = Console.ReadLine();

                cmd.Parameters.Add(parameter1);
                cmd.Parameters.Add(parameter2);
                cmd.Parameters.Add(parameter3);

                conn.Open();
                cmd.ExecuteNonQuery();
                Console.WriteLine("Record inserted Successfully....." );

            }
            catch(Exception ex)
            {
                Console.WriteLine("Record Insertion Failed using Stored Procedure...");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
