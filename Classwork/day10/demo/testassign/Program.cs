﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testassign
{
    public delegate string mydelegate(); 
    class Program
    {
        static void Main(string[] args)
        {
            A aobj = new A();
            B bobj = new B();
            mydelegate ptr = new mydelegate(bobj.M2);

            aobj.m1(ptr);
            Console.ReadLine();
        }
    }

    public class A
    {
        #region condition

        //You can pass one parameter to M1
        //but parameter shoud not be of type string
        //parameter shoud not be of type  B 
        //parameter shoud not be of type Object

        // Here you will have to call M2 method from B class
        //and print here what M2 returns on the screen..

        // but conditions are: Here in this code of M1
        // 1. You wil not declare B Object
        // 2. No usage of Generics / Inheritance / Overloading  OverRiding 
        // 3. No Usage of Static / abstract / singletone
        // 4. No Usage of Collections
        // 5. No Events
        // 6. No File Io / Serialization
        // 7. No Partial Class, Nullbale Type, Anonymous Method, Lambada Expression to be used 
        // 8. No DLL Concept 

        #endregion

        public void m1(mydelegate ptr)
        {
            Console.WriteLine(ptr());
        }


    }

    public class B
    {
        public string M2()
        {
            return "M2 from B";
        }
    }
}
