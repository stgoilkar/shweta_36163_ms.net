﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lambdaExpression
{
    delegate string mydelegate(string s);
    class Program
    {
        static void Main(string[] args)
        {
            mydelegate ptr = (s) =>
            {
                return "Helloo " + s;
            };
            Console.WriteLine(ptr("Name"));
            Console.ReadLine();
        }
    }
}
