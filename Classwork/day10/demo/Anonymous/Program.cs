﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous
{
    delegate string mydelegate(string name);
    class Program
    {
        static void Main(string[] args)
        {
            #region Normal method call
            //Console.WriteLine(Sayhi("Shweta")); 
            #endregion

            #region method using pointer delegates
            //mydelegate ptr = new mydelegate(Sayhi);
            //Console.WriteLine(ptr("Shweta")); 
            #endregion

            #region anonymaous method
            mydelegate ptr = delegate (string s)
              {
                  return "Hello " + s;
              };

            Console.WriteLine(ptr("Sunbeam"));
            #endregion

            Console.ReadLine();
        }

        public static string Sayhi(string name)
        {
            return "Hiiiii " + name;
        }
    }
}
