﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region partial class
            Maths m1 = new Maths();
            Console.WriteLine(m1.Add(10, 20));
            Console.WriteLine(m1.Sub(10, 20));
            Console.WriteLine(m1.Mult(10, 20));
            Console.ReadLine(); 
            #endregion
        }
    }
}
