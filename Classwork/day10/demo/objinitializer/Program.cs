﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace objinitializer
{
    class Program
    {
        static void Main(string[] args)
        {
            Emp emp = new Emp() { No = 1, Name = "Shweta", Address = "Kolhapur" };
            Console.WriteLine("NO : " +emp.No +" Name : " +emp.Name +" Address : " +emp.Address);
            Console.ReadLine();
        }
    }

    public class Emp
    {
        public int No { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
