﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoProperty
{
    class Program
    {
        static void Main(string[] args)
        {
            Emp emp = new Emp();
            emp.No = 1;
            emp.Name = "shweta";
            emp.Address = "Kolhapur";

            Console.WriteLine("Employee No : " +emp.No +
                        " , Name : "+emp.Name+
                        " , Address : " +emp.Address);
            Console.ReadLine();
        }
    }

    public class Emp
    {
        public int No { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
