﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImplicitType
{
    class Program
    {
        static void Main(string[] args)
        {
            var v1 = 100;
            Console.WriteLine(v1);
            //v1 = "name";

            var v2 = "name";
            Console.WriteLine(v2);
           
            Console.ReadLine();
        }
    }
}
