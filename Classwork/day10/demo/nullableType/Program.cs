﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nullableType
{
    class Program
    {
        static void Main(string[] args)
        {
            //Nullable<int> Salary = null;
            int? Salary = 100;
            if (Salary.HasValue)
            {
                Console.WriteLine("Salary holds a value");
            }
            else
            {
                Console.WriteLine("Salary is Null");
            }
            Console.ReadLine();
        }
    }
}
