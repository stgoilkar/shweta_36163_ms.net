﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace objectInitializer
{
    class Program
    {
        static void Main(string[] args)
        {
            var obj1 = new { No = 1, Name = "Shweta", Age = 24 };
            Console.WriteLine(obj1);

            var obj2 = new { No = 2, Name = "Rhea", Age ="forty" };
            Console.WriteLine(obj2);

            var obj3 = new {  Name = "Prachi", No = 3, Age = 12 };
            Console.WriteLine(obj3);

            Console.ReadLine();
        }
    }
}
