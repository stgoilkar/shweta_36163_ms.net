﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC.Models;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class TestController : BaseController
    {
        // GET: Test
        public ActionResult Test(int? id)
        {
            ActionResult result = null;
            switch (id)
            {
                case 1:
                    Emp emp = new Emp() { Id = 1, Name = "Shweta", Address = "Kolhapur" };
                    result = View(emp);
                    break;
                case 2:
                    result = new JsonResult() { Data = dbObj.Emps.ToList(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                    break;
                case 3:
                    result = new JavaScriptResult() { Script="alert('Hello from JS')"};
                    break;
                case 4:
                    result = View("Display");
                    break;
                default:
                    result =new  ContentResult(){ ContentType="text/plain",Content="Hello from Server" };
                    break;
            }
           
            return result;
        }
    }
}