﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee e1 = new Employee();
            e1.No = 1;
            e1.Name = "Shweta";
            e1.DeptName = "IT";

            Employee e2 = new Employee();
            e2.No = 2;
            e2.Name = "Sayli";
            e2.DeptName = "IT";

            Employee e3 = new Employee();
            e3.No = 3;
            e3.Name = "Pooja";
            e3.DeptName = "Mech";

            Customer c1 = new Customer();
            c1.No = 4;
            c1.Name = "Shradha";
            c1.OrderDetails = "Ram";

            Customer c2 = new Customer();
            c2.No = 5;
            c2.Name = "Shree";
            c2.OrderDetails = "Mobile";


            Person[] person = new Person[5];
            person[0] = e1;
            person[1] = e2;
            person[2] = e3;
            person[3] = c1;
            person[4] = c2;

            for (int i = 0; i < person.Length; i++)
            {
                if (person[i] != null)
                {
                    Console.WriteLine(person[i].getDetails());
                }
            }

            Console.ReadLine();
        }
    }

    public class Person
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public virtual string getDetails()
        {
            return this.No.ToString() + " " + this.Name;
        }
    }

    public class Employee:Person
    {

        private string _DeptName;

        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }

        public override string getDetails()
        {
            return base.getDetails() + " " + this.DeptName;
        }
    }

    public class Customer :Person
    {
        private string _OrderDetails;

        public string OrderDetails
        {
            get { return _OrderDetails; }
            set { _OrderDetails = value; }
        }

        public override string getDetails()
        {
            return base.getDetails()+ " " + this.OrderDetails;
        }
    }
}
