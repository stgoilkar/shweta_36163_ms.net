﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace _07demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee emp1 = new Employee();
            emp1.No = 1;
            emp1.Name = "Shweta";

            Hashtable arr = new Hashtable();
            arr.Add("a",100);
            arr.Add("b", "Shweta");
            arr.Add("c", true);
            arr.Add("d", 10.5);
            arr.Add("e", emp1);
            arr.Add("f", new DateTime());
            arr.Add("g", new int[] { 10, 20, 30, 40, 50, 60 });

            foreach (object key in arr.Keys)
            {
                Console.WriteLine(key);
                object obj = arr[key];
                if (obj is string)
                {
                    Console.WriteLine(Convert.ToString(obj));
                }
                else if (obj is int)
                {
                    Console.WriteLine(Convert.ToInt32(obj));
                }
                else if (obj is bool)
                {
                    Console.WriteLine(Convert.ToBoolean(obj));
                }
                else if (obj is Employee)
                {
                    Console.WriteLine(((Employee)obj).GetDetails());
                }
                else
                {
                    Console.WriteLine("Unknown Data type");
                }
            }

            Console.ReadLine();
        }
    }


    public class Employee
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string GetDetails()
        {
            return this.No.ToString() + " " + this.Name;
        }
    }
}
