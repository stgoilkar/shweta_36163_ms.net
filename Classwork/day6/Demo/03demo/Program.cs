﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03demo
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[5];
            arr[0] = 1;
            arr[1] = 10;
            arr[2] = 100;
            arr[3] = 1000;
            arr[4] = 10000;

            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(arr[i]);
            }



            int[] arr1 = new int[] { 2, 20, 200, 2000 };
            for (int i = 0; i < arr1.Length; i++)
            {
                Console.WriteLine(arr1[i]);
            }



            Employee e1 = new Employee();
            e1.No = 1;
            e1.Name = "Rajiv";

            Employee e2 = new Employee();
            e2.No = 2;
            e2.Name = "Rahul";

            Employee e3 = new Employee();
            e3.No = 3;
            e3.Name = "Mahesh";

            Employee[] emp = new Employee[3];
            emp[0] = e1;
            emp[1] = e2;
            emp[2] = e3;

            for (int i = 0; i < emp.Length; i++)
            {
                Console.WriteLine(emp[i].GetDetails());
            }

            Console.ReadLine();
        }
    }

    public class Employee
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string GetDetails()
        {
            return this.No.ToString() + " " + this.Name;
        }
    }
}
