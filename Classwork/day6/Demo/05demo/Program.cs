﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace _05demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee emp1 = new Employee();
            emp1.No = 1;
            emp1.Name = "Shweta";

            ArrayList arr = new ArrayList();
            arr.Add(100);
            arr.Add("shweta");
            arr.Add(10.2);
            arr.Add(true);
            arr.Add(emp1);

            for (int i = 0; i < arr.Count; i++)
            {
                if (arr[i] is string)
                {
                    Console.WriteLine(Convert.ToString(arr[i]));
                }
                else if (arr[i] is int)
                {
                    Console.WriteLine(Convert.ToInt32(arr[i]));
                }
                else if (arr[i] is bool)
                {
                    Console.WriteLine(Convert.ToBoolean(arr[i]));
                }
                else if (arr[i] is Employee)
                {
                    Console.WriteLine(((Employee)arr[i]).GetDetails());
                }
                else
                {
                    Console.WriteLine("Unknown Data type");
                }
            }
            Console.ReadLine();
        }
    }

    public class Employee
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string GetDetails()
        {
            return this.No.ToString() + " " + this.Name;
        }
    }
}
