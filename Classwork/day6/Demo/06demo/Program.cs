﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace _06demo
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList arr = new ArrayList();
            string isContinue = "y";
            do
            {
                Console.WriteLine("Whose Data you want to enter");
                Console.WriteLine("1 . Employee , 2 : Book");

                int choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        arr.Add(EmployeeEnter());
                        break;
                    case 2:
                        arr.Add(BookEnter());
                        break;
                    default:
                        Console.WriteLine("Enter Valid input");
                        break;
                }
                Console.WriteLine("Do you want to continue...? y/n");
                isContinue = Console.ReadLine();

            } while (isContinue == "y");

            Console.WriteLine("******************************************");
            Console.WriteLine("You Enter below information");

            for (int i = 0; i < arr.Count; i++)
            {
                if (arr[i] is Employee)
                {
                    Console.WriteLine(((Employee)arr[i]).GetDetails());
                }
                else if (arr[i] is Book)
                {
                    Console.WriteLine(((Book)arr[i]).GetBookDetails());
                }
            }

            Console.ReadLine();
        }

        public static Employee EmployeeEnter()
        {
            Employee e = new Employee();
            Console.WriteLine("Enter the Number");
            e.No = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the Name");
            e.Name = Console.ReadLine();
            return e;
        }

        public static Book BookEnter()
        {
            Book b = new Book();
            Console.WriteLine("Enter the Title");
            b.Title = (Console.ReadLine());

            Console.WriteLine("Enter the Author");
            b.Author = Console.ReadLine();
            return b;
        }
    }

    public class Employee
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string GetDetails()
        {
            return this.No.ToString() + " " + this.Name;
        }
    }

    public class Book
    {
        private string _Title;
        private string _Author;

        public string Author
        {
            get { return _Author; }
            set { _Author = value; }
        }


        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public string GetBookDetails()
        {
            return this.Title + " " + this.Author;
        }
    }
}
