﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 
            Employee emp1 = new Employee();
            emp1.No = 1;
            emp1.Name = "Shweta";

            Object obj = new Object();
            obj = 100;
            //obj = emp1;
            //obj = "shweta";
            obj = true;

            if(obj is int)
            {
                Console.WriteLine(Convert.ToInt32(obj));
            }
            else if(obj is string)
            {
                Console.WriteLine(Convert.ToString(obj));
            }
            else if(obj is Employee)
            {
                Console.WriteLine(((Employee)obj).GetDetails());
            }
            else
            {
                Console.WriteLine("Unknown Data type......");
            }

            Console.ReadLine();
            #endregion
        }

        public class Employee
        {
            private int _No;
            private string _Name;

            public string Name
            {
                get { return _Name; }
                set { _Name = value; }
            }

            public int No
            {
                get { return _No; }
                set { _No = value; }
            }

            public string GetDetails()
            {
                return this.No.ToString() + " " + this.Name;
            }
        }
    }
}
