﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Linq.Expressions;

namespace LambadaExpression
{
    public delegate bool myDelegate(int i);
    public delegate P myDelegate<P, Q>(Q i);
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch watch = new Stopwatch();

            #region Normal Method call
            //watch.Start();

            //bool result=Check(1);
            //watch.Stop();

            //Console.WriteLine("Time taken = "+ watch.ElapsedTicks.ToString());  //4096
            //Console.WriteLine(result); 
            #endregion

            #region using delegate in normal method call
            //myDelegate ptr = new myDelegate(Check);

            //watch.Start();
            //bool result=ptr(100);
            //watch.Stop();
            //Console.WriteLine("Time taken = " + watch.ElapsedTicks.ToString());  //3410
            //Console.WriteLine(result);  
            #endregion

            #region Anonymous function call using delegates
            //myDelegate ptr = delegate (int i)
            //  {
            //      return (i > 100);
            //  };

            //watch.Start();
            //bool result = ptr(10);
            //watch.Stop();
            //Console.WriteLine("Time taken = " + watch.ElapsedTicks.ToString());  
            //Console.WriteLine(result);   
            #endregion

            #region Lambada Expression
            //myDelegate ptr = (int i)=>
            //{
            //    return (i > 100);
            //};

            //watch.Start();
            //bool result = ptr(10);
            //watch.Stop();
            //Console.WriteLine("Time taken = " + watch.ElapsedTicks.ToString());
            //Console.WriteLine(result);
            #endregion

            #region Generic Delegate
            //myDelegate<bool, int> ptr = new myDelegate<bool, int>(Check);

            //myDelegate<bool, int> ptr = delegate (int i)
            //   {
            //       return (i > 10);
            //   };

            //myDelegate<bool, int> ptr = (int i)=>
            //{
            //    return (i > 10);
            //};
            //watch.Start();
            //bool result = ptr(100);
            //watch.Stop();
            //Console.WriteLine("Time taken = " + watch.ElapsedTicks.ToString());
            //Console.WriteLine(result); 
            #endregion

            #region Func using delegate
            //Func<int, bool> ptr =delegate (int i) 
            //{
            //    return (i > 10);
            //};

            //Func<int, bool> ptr = (i) => { return (i > 10); };
            //watch.Start();
            //bool result = ptr(100);
            //watch.Stop();
            //Console.WriteLine("Time taken = " + watch.ElapsedTicks.ToString());
            //Console.WriteLine(result); 
            #endregion

            Expression<Func<int, bool>> tree = (i) => (i > 10);
            Func<int, bool> ptr = tree.Compile();

            watch.Start();
            bool result = ptr(100);
            watch.Start();

            Console.WriteLine("Time taken = " + watch.ElapsedTicks.ToString());
            Console.WriteLine(result); 

            Console.ReadLine();
        }

        public static bool Check(int i)
        {
            return (i > 10);
        }
    }
}
