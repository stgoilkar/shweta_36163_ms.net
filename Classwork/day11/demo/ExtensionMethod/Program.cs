﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Enter the Email : ");
            //string data = Console.ReadLine();

            //Utility utility = new Utility();
            //bool result = utility.CheckForValidEmail(data);

            //bool result = Utility.CheckForValidEmail(data);

            //bool result = Utility.CheckForValidEmail(data, 1);

            //bool result = data.CheckForValidEmail(100);
            //Console.WriteLine(result);

            int[] arr = new int[] { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
            Console.WriteLine(arr.Average());

            var v = new { No = 1, Name = "Shweta", Address = "Pune" };
            Console.WriteLine(v.CheckForValidEmail(1));
            Console.ReadLine();

        }
    }

    public static class Utility{
        public static bool CheckForValidEmail(this string data,int i)
        {
            return data.Contains("@");
        }
        public static bool CheckForValidEmail<T>(this T data, int i)
        {
            return true;
        }
    }

        
    #region sealed class
    public class A
    {
        public virtual void M1()
        {

        }
    }

    public class B : A
    {
        public sealed override void M1()
        {
            base.M1();
        }
    }

    public class C : B
    {
        //not allowed
        //public override void M1()
        //{
        //    base.M1();
        //}
    } 
    #endregion
}
