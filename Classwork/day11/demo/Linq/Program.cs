﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Emp> emps = new List<Emp>()
            {
                new Emp{No=1,Name="shweta",Address="Titwala"},
                new Emp{No=2,Name="Srushti",Address="Ratnagiri"},
                new Emp{No=3,Name="Prachi",Address="Vada"},
                new Emp{No=4,Name="Rutuja",Address="Kalyan"},
                new Emp{No=6,Name="Aarti",Address="Satara"},
                new Emp{No=7,Name="Harshada",Address="Bosari"},
                new Emp{No=8,Name="Ashutosh",Address="Satara"},
                new Emp{No=9,Name="Ameya",Address="Alibag"},
                new Emp{No=10,Name="Atul",Address="Gujrat"},
                new Emp{No=11,Name="Abhishek",Address="Alibag"},
                new Emp{No=12,Name="Dipesh",Address="Uttar Pradesh"}
            };

            Console.WriteLine("Enter Address Search Character : ");
            string address = Console.ReadLine();

            #region Address filter using foreach
            //var result =new List<Emp>();

            //foreach (Emp emp in emps)
            //{
            //    if(emp.Address.StartsWith(address))
            //    {
            //        result.Add(emp);
            //    }
            //}
            //foreach (Emp res in result)
            //{
            //    Console.WriteLine("No : " + res.No+ " , Name : " +res.Name+" , Address : "+res.Address);
            //} 
            #endregion

            #region Passing whole object 
            //var result = (from emp in emps
            //              where emp.Address.StartsWith(address)
            //              select emp).ToList();
            //foreach (var res in result)
            //{
            //    Console.WriteLine("No : " + res.No+ " , Name : " +res.Name+" , Address : "+res.Address);
            //} 
            #endregion

            #region passing object of new resultEmp in select
            //var result = (from emp in emps
            //              where emp.Address.StartsWith(address)
            //              select new
            //              {
            //                  RName = "Mr / Mrs " + emp.Name,
            //                  RAddress = emp.Address
            //              }).ToList();

            //foreach (var res in result)
            //{
            //    Console.WriteLine("Name : " + res.RName + " , Address : " + res.RAddress);
            //}

            #endregion

            var result = (from emp in emps
                          where emp.Address.StartsWith(address)
                          select emp);

            emps.Add(new Emp { No = 15, Name = "Dharmik", Address = "Satara" });

            foreach (var res in result)
            {
                Console.WriteLine("name : " + res.Name + " , address : " + res.Address);
            }

            Console.ReadLine();
        }
    }

    public class resultEmp
    {
        public string RName { get; set; }
        public string RAddress { get; set; }
    }
    
    public class Emp
    {
        public int No { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
