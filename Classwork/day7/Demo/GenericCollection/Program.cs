﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Creating Employee object
            Employee emp1 = new Employee();
            emp1.No = 1;
            emp1.Name = "Shweta";

            Employee emp2 = new Employee();
            emp2.No = 2;
            emp2.Name = "Swapnali";

            Employee emp3 = new Employee();
            emp3.No = 3;
            emp3.Name = "Sayali";
            #endregion

            #region List

            //List<Employee> arr = new List<Employee>();
            //arr.Add(emp1);
            //arr.Add(emp2);
            //arr.Add(emp3);

            //foreach (Employee employee in arr)
            //{
            //    Console.WriteLine(employee.GetDetails());
            //}
            #endregion

            #region List as ArrayList

            //List<object> obj = new List<object>();
            //List<Utility<int>> utilities = new List<Utility<int>>();
            #endregion

            #region Stack

            //Stack<Employee> arr = new Stack<Employee>();
            //arr.Push(emp1);
            //arr.Push(emp2);
            //arr.Push(emp3);

            //foreach (Employee employee in arr)
            //{
            //    Console.WriteLine(employee.GetDetails());
            //}

            //Console.WriteLine(arr.Peek().GetDetails());
            //Console.WriteLine(arr.Pop().GetDetails());
            #endregion

            #region Queue

            //Queue<Employee> arr = new Queue<Employee>();
            //arr.Enqueue(emp1);
            //arr.Enqueue(emp2);
            //arr.Enqueue(emp3);


            //foreach (Employee employee in arr)
            //{
            //    Console.WriteLine(employee.GetDetails());
            //}

            //Console.WriteLine(arr.Dequeue().GetDetails());
            //Console.WriteLine(arr.Dequeue().GetDetails());
            //Console.WriteLine(arr.Dequeue().GetDetails());
            #endregion

            #region Dictonary
            Dictionary<int, Employee> arr = new Dictionary<int, Employee>();

            arr.Add(emp1.No, emp1);
            arr.Add(emp2.No, emp2);
            arr.Add(emp3.No, emp3);

            Console.WriteLine(arr[3].GetDetails());

            foreach (int key in arr.Keys)
            {
                Console.WriteLine("Data available at key " + key);
                Employee employee = arr[key];
                Console.WriteLine(employee.GetDetails());
            }
            #endregion

            Console.ReadLine();

        }
    }

    public class Utility<T> { }
    public class Employee
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string GetDetails()
        {
            return "No : " + this.No.ToString() + " Name : " + this.Name;
        }
    }
}
