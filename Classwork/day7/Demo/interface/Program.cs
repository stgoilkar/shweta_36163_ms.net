﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @interface
{
    class Program
    {
        static void Main(string[] args)
        {
            #region database
            /*            SQL sql = new SQL();
                sql.Insert();
                sql.Update();
                sql.Delete();*/
            #endregion

            A obj = new C();
            Console.WriteLine(obj.Add(10, 20));
            Console.WriteLine(obj.Sub(20, 10));

            B obj1 = new C();
            Console.WriteLine(obj1.Add(10, 20));
            Console.WriteLine(obj1.Mul(20, 10));

            A t1 = new Test();
            Console.WriteLine(t1.Add(10, 0));

            Experiment t2 = new Test();
            Console.WriteLine(t2.Sub(20, 10));

            Console.ReadLine();
        }
    }

    public interface A
    {
        int Add(int x, int y);
        int Sub(int x, int y);
    }

    public interface B
    {
        int Add(int x, int y);
        int Mul(int x, int y);
    }

    public class C : A, B
    {
        int A.Add(int x, int y)
        {
            return x + y;
        }

        int B.Add(int x, int y)
        {
            return x + y + 1000;
        }

        int B.Mul(int x, int y)
        {
            return x * y;
        }

        int A.Sub(int x, int y)
        {
            return x - y;
        }
    }

    public abstract class Experiment : A
    {
        public abstract int Add(int x, int y);
        public virtual int Sub(int x, int y)
        {
            return x - y;
        }


    }

    public class Test : Experiment
    {
        public override int Add(int x, int y)
        {
            return x + y + 10000;
        }

        public override int Sub(int x,int y)
        {
            return x - y - 10;
        }
    }

    #region Database
    public interface Database
    {
        void Insert();

        void Update();

        void Delete();
    }

    public class SQL : Database
    {
        public void Insert()
        {
            Console.WriteLine("Sql Insert........");
        }

        public void Update()
        {
            Console.WriteLine("Sql update........");
        }

        public void Delete()
        {
            Console.WriteLine("Sql Delete........");
        }
    }

    public class Oracle : Database
    {
        public void Insert()
        {
            Console.WriteLine("Oracle Insert........");
        }

        public void Update()
        {
            Console.WriteLine("Oracle update........");
        }

        public void Delete()
        {
            Console.WriteLine("Oracle Delete........");
        }
    }
    #endregion

}
