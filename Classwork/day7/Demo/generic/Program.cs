﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace generic
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Simple Swap Logic
            //Maths m1 = new Maths();
            //int x = 10;
            //int y = 30;

            //Console.WriteLine("Before Swap x = " +x +" , y = " +y);
            //m1.Swap(ref x, ref y);
            //Console.WriteLine("After Swap x = " + x + " , y = " + y);
            //Console.ReadLine(); 
            #endregion

            #region Swap of Double
            //Maths<double> m1 = new Maths<double>();
            //double x = 10.20;
            //double y = 30.40;

            //Console.WriteLine("Before Swap x = " + x + " , y = " + y);
            //m1.Swap(ref x, ref y);
            //Console.WriteLine("After Swap x = " + x + " , y = " + y);
            //Console.ReadLine(); 
            #endregion

            #region normal class with generic method
            //Math m1 = new Math();

            //Console.WriteLine(m1.Add(100, 200));
            //int x = 10;
            //int y = 30;

            //Console.WriteLine("Before Swap x = " + x + " , y = " + y);
            //m1.Swap<int>(ref x, ref y);
            //Console.WriteLine("After Swap x = " + x + " , y = " + y);
            //Console.ReadLine(); 
            #endregion

            #region Using Normal Derived Class object to call Generic Base Class Method
            //newMaths n1 = new newMaths();

            //int x = 10;
            //int y = 30;

            //Console.WriteLine("Before Swap x = " + x + " , y = " + y);
            //n1.Swap(ref x, ref y);
            //Console.WriteLine("After Swap x = " + x + " , y = " + y);
            //Console.ReadLine(); 
            #endregion

            SpecialMaths<int,float,double,int> sm = new SpecialMaths<int, float, double, int>();

            double value = sm.DoSomething(10, 40, 100.22, 20);

            Console.WriteLine(value);
            Console.ReadLine();
        }


    }

    public class Maths<T>
    {
        public void Swap(ref T x, ref T y)
        {
            T z = x;
            x = y;
            y = z;
        }
    }

    public class Math
    {
        public void Swap<T>(ref T x, ref T y)
        {
            T z = x;
            x = y;
            y = z;
        }

        public int Add(int x,int y)
        {
            return x + y;
        }
    }

    public class newMaths : Maths<int>
    {

    }

    public class SpecialMaths<a, b, c, d> : Maths<d>
    {
        public c DoSomething(a x,b y,c z,d e)
        {
            return z;
        }
    }
}
