﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace fileIo
{
    class Program
    {
        static void Main(string[] args)
        {

            #region File Writing

            //FileStream fs = new FileStream(@"C:\CDAC\MS.NET\shweta_36163_ms.net\Classwork\day7\try.txt", FileMode.OpenOrCreate, FileAccess.Write);

            //StreamWriter writer = new StreamWriter(fs);
            //writer.WriteLine("Hello Sunbeam");

            //fs.Flush();
            //writer.Close();
            //fs.Close();
            #endregion


            #region File Reading

            //FileStream fs = new FileStream(@"C:\CDAC\MS.NET\shweta_36163_ms.net\Classwork\day7\try.txt", FileMode.Open, FileAccess.Read);

            //StreamReader reader = new StreamReader(fs);
            ////Console.WriteLine(reader.ReadToEnd());

            //while (true)
            //{
            //    string data = reader.ReadLine();
            //    if (data != null)
            //    {
            //        Console.WriteLine(data);
            //    }
            //    else
            //    {
            //        break;
            //    }
            //}
            //Console.ReadLine();

            //fs.Flush();
            //reader.Close();
            //fs.Close();
            #endregion


            #region Employee Writing
            Employee emp = new Employee();
            Console.WriteLine("Enter No :");
            emp.No = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Name : ");
            emp.Name = Console.ReadLine();

            FileStream fs = new FileStream(@"C:\CDAC\MS.NET\shweta_36163_ms.net\Classwork\day7\try.txt", FileMode.OpenOrCreate, FileAccess.Write);

            StreamWriter writer = new StreamWriter(fs);
            writer.WriteLine(emp);

            fs.Flush();
            writer.Close();
            fs.Close(); 
            #endregion
        }
    }


    public class Employee
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return "No : "+ No.ToString()+" , Name :  " + Name;
        }

    }
}
