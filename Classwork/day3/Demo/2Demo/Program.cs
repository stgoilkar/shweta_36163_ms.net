﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            string isContinue = "y";
            do
            {
                Console.WriteLine("Enter Choice Number");
                Console.WriteLine("1:Add, 2:Sub , 3:Mul, 4:Div");
                int choice = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter First Value ");
                int x = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter Second Value ");
                int y = Convert.ToInt32(Console.ReadLine());

                int result = 0;

                switch (choice)
                {
                    case 1:
                        result = x + y;
                        break;
                    case 2:
                        result = x - y;
                        break;
                    case 3:
                        result = x * y;
                        break;
                    case 4:
                        result = x / y;
                        break;
                    default:
                        Console.WriteLine("Invalid Choice...");
                        break;
                }
                Console.WriteLine(result);
                Console.WriteLine("Would you like to continue? y/n");
                isContinue = Console.ReadLine();
            } while (isContinue == "y");
            Console.ReadLine();
        }
    }
}
