﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = Maths();
            Console.WriteLine(result);
            Console.ReadLine();
        }

        static int Maths()
        {
            Console.WriteLine("Enter first Number");
            int x = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter second Number");
            int y = Convert.ToInt32(Console.ReadLine());

            return x + y;
        }
    }
}
