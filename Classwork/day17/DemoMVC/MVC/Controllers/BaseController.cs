﻿using MVC.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    [CustomFilter]
    [Authorize]
    [HandleError(ExceptionType =typeof(Exception),View ="Error")]
    public class BaseController : Controller
    {
        protected sunbeamDBEntities dbObj { get; set; }

        public BaseController()
        {
            this.dbObj = new sunbeamDBEntities();
        }
    }
}