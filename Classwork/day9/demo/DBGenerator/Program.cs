﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using MyOwnAttributesLib;

namespace DBGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the Path : ");
            string path = Console.ReadLine();

            Assembly assembly = Assembly.LoadFrom(path);

            Type[] allType = assembly.GetTypes();
            string query = "";

            foreach (Type type in allType)
            {
                List<Attribute> allattributes = type.GetCustomAttributes().ToList();
                foreach (Attribute attribute in allattributes)
                {
                    if(attribute is Table)
                    {
                        Table TableAttributeObject = (Table)attribute;
                        string tableName = TableAttributeObject.TableName;
                        query = query + " create table " + tableName + " ( ";
                        break;
                    }
                }
                PropertyInfo[] allGetterSetter = type.GetProperties();
                foreach (PropertyInfo property in allGetterSetter)
                {
                    List<Attribute> allgettersetterattributes = property.GetCustomAttributes().ToList();
                    foreach (Attribute attribute in allgettersetterattributes)
                    {
                        if (attribute is Column)
                        {
                            Column columnAttribute = (Column)attribute;
                            query = query + columnAttribute.ColumnName + " " + columnAttribute.ColumnType + ",";
                            break;
                        }
                    }
                }
                query= query.TrimEnd(new char[] { ',' });
                query += " ); ";
            }
                Console.WriteLine(query);
                Console.ReadLine();
        }
    }
}
