﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the Assembly Path : ");
            string pathofAssembly = Console.ReadLine();
            Assembly assembly = Assembly.LoadFrom(pathofAssembly);

            Type[] allTypes = assembly.GetTypes();
            object dynamicObject = null;

            foreach (Type type in allTypes)
            {
                Console.WriteLine("Type : " +type.Name);
                dynamicObject = assembly.CreateInstance(type.FullName);

                MethodInfo[] allMethods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                foreach (MethodInfo methodInfo in allMethods)
                {
                    Console.WriteLine(methodInfo.ReturnType +" " +methodInfo.Name + " ( ");
                    ParameterInfo[] allParams = methodInfo.GetParameters();

                    foreach (ParameterInfo parameterInfo in allParams)
                    {
                        Console.WriteLine(parameterInfo.ParameterType.ToString()+ " "+parameterInfo.Name+ " ");
                    }
                    Console.Write(")");
                    Console.WriteLine();

                    object[] allParam = new object[] { 10, 20 };
                    object result = type.InvokeMember(methodInfo.Name,
                                BindingFlags.Public |
                                BindingFlags.Instance |
                                BindingFlags.InvokeMethod,
                                null, dynamicObject, allParam);

                    Console.WriteLine("Result of " +methodInfo.Name+" executed is "+result.ToString());

                    Console.ReadLine();
                }
            }
        }
    }
}
