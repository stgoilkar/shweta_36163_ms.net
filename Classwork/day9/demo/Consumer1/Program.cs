﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Consumer1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the Assembly Path : ");
            string pathofAssembly = Console.ReadLine();

            Assembly assembly = Assembly.LoadFrom(pathofAssembly);

            Type[] allType = assembly.GetTypes();

            object dynamicObject = null;
            foreach (Type type in allType)
            {
                Console.WriteLine("Type : " +type.Name);

                List<Attribute> allAttributes = type.GetCustomAttributes().ToList();
                bool isItSerializable = false;
                foreach (Attribute attribute in allAttributes)
                {
                    if(attribute is SerializableAttribute)
                    {
                        isItSerializable = true;
                        break;
                    }
                }
                if (isItSerializable)
                {
                    Console.WriteLine(type.Name + "is Serializable");
                }
                else
                {
                    Console.WriteLine(type.Name + "is not Serializable");
                }

                dynamicObject = assembly.CreateInstance(type.FullName);

                MethodInfo[] allMethod = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                foreach (MethodInfo methodInfo in allMethod)
                {
                    Console.WriteLine("Calling " +methodInfo.Name + "method....");
                    ParameterInfo[] allParams = methodInfo.GetParameters();

                    object[] argument = new object[allParams.Length];

                    for (int i = 0; i < allParams.Length; i++)
                    {
                        Console.WriteLine("Enter " + allParams[i].ParameterType.ToString() +" Value for " +allParams[i].Name);

                        argument[i] = Convert.ChangeType(Console.ReadLine(), allParams[i].ParameterType);

                    }
                        object result = type.InvokeMember(methodInfo.Name,
                                        BindingFlags.Public | BindingFlags.Instance |
                                        BindingFlags.InvokeMethod, null, dynamicObject, argument);

                        Console.WriteLine("Result of " +methodInfo.Name + " executed is " +result.ToString());
                    Console.ReadLine();
                }
            }
        }
    }
}
