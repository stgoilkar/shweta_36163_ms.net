﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathsLib
{
    [Serializable]
    public class MathsAdd
    {
        public int Add(int x, int y)
        {
            return x + y;
        }

    }

    [Serializable]
    public class MathsSub
    {

        public int Sub(int x, int y)
        {
            return x - y;
        }
    }

}
