﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delegateTest
{
    delegate int myDelegate(int x, int y);
    delegate void yourDelegate(string s);
    class Program
    {
        static void Main(string[] args)
        {
            myDelegate ptr = new myDelegate(Add);
            int result = ptr(10, 20);
            Console.WriteLine("Addition : " +result);

            myDelegate ptr1 = new myDelegate(Sub);
            int result1 = ptr1(10, 20);
            Console.WriteLine("Subtraction : " + result1);

            yourDelegate ptr2 = new yourDelegate(SayHii);
            ptr2("Shweta");
            Console.ReadLine();
        }

        public static int Add(int x,int y)
        {
            return x + y;
        }
        public static int Sub(int x, int y)
        {
            return x - y;
        }

        public static void SayHii(string Name)
        {
            Console.WriteLine("Hiii " +Name);
        }
    }
}
