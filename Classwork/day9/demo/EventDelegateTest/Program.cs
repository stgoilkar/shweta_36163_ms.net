﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB;

namespace EventDelegateTest
{
    class Program
    {
        static void Main(string[] args)
        {
            SQLServer sql = new SQLServer();

            mydelegate ptr1 = new mydelegate(onInserted);
            mydelegate ptr2 = new mydelegate(onUpdated);
            mydelegate ptr3 = new mydelegate(onDeleted);

            sql.Inserted += ptr1;
            sql.Updated += ptr2;
            sql.Deleted += ptr3;

            sql.Insert("Hello");
            sql.Update("Hello");
            sql.Delete("Hello");

            Console.ReadLine();
        }

        public static void onInserted()
        {
            Console.WriteLine("Logging Inserted into Console");
        }
        public static void onUpdated()
        {
            Console.WriteLine("Logging Updated into Console");
        }
        public static void onDeleted()
        {
            Console.WriteLine("Logging Deleted into Console");
        }
    }
}

namespace DB
{
    public delegate void mydelegate();
    public class SQLServer
    {
        public event mydelegate Inserted;
        public event mydelegate Updated;
        public event mydelegate Deleted;
        public void Insert(string Data)
        {
            Console.WriteLine(Data + " inserted into SQL Server");
            Inserted();
        }
        public void Update(string Data)
        {
            Console.WriteLine(Data + " updated into SQL Server");
            Updated();
        }
        public void Delete(string Data)
        {
            Console.WriteLine(Data + " deleted into SQL Server");
            Deleted();
        }
    }
}
