﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the Choice");
            Console.WriteLine("1: PDF , 2: Excel , 3:Word");
            int choice = Convert.ToInt32(Console.ReadLine());


            switch (choice)
            {
                case 1:
                    PDF();
                    break;
                case 2:
                    Excel();
                    break;
                case 3:
                    Word();
                    break;
                default:
                    break;
            }
            Console.ReadLine();
        }

         static public void PDF()
        {
            Console.WriteLine("PDF parsing is successfully Completed");
            Console.WriteLine("PDF Validation is successfully Completed");
            Console.WriteLine("PDF Saving is successfully Completed");
        }

        static public void Excel()
        {
            Console.WriteLine("Excel parsing is successfully Completed");
            Console.WriteLine("Excel Validation is successfully Completed");
            Console.WriteLine("Excel Saving is successfully Completed");
        }

        static public void Word()
        {
            Console.WriteLine("Word parsing is successfully Completed");
            Console.WriteLine("Word Validation is successfully Completed");
            Console.WriteLine("Word Saving is successfully Completed");
        }
    }
}
