﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Second;
using Second.Third;

namespace _0demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Maths m = new Maths();
            Console.WriteLine(m.add(20,30));

            Test t = new Test();
            Console.WriteLine(t.Sample());

            Database db = new Database();
            db.Insert();

            Console.ReadLine();
        }
    }
}

namespace Second
{
    public class Maths
    {
        public int add(int x, int y)
        {
            return x + y;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Entry Point function in maths class");
        }
    }

     public class Test
     {
        public string Sample() 
        {
            return "this is Sample test";
        }
    }

    namespace Third
    {
        public class Database
        {
            static void Main(string[] args)
            {
                Console.WriteLine("Entry Point function in Database class");
            }

            public void Insert()
            {
                Console.WriteLine("Data Inserted");
            }
        }
    }
}
