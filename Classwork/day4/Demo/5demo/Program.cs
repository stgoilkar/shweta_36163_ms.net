﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the Choice");
            Console.WriteLine("1: PDF , 2: Excel , 3:Word , 4:Test");
            int choice = Convert.ToInt32(Console.ReadLine());


            switch (choice)
            {
                case 1:
                    PDF obj = new PDF();
                    obj.Generate();
                    break;
                case 2:
                    Excel obj1 = new Excel();
                    obj1.Generate();
                    break;
                case 3:
                    Word obj2 = new Word();
                    obj2.Generate();
                    break;
                case 4:
                    Test obj3 = new Test();
                    obj3.Generate();
                    break;
                default:
                    break;
            }
            Console.ReadLine();
        }
    }

    public abstract class FinalResult
    {
        protected abstract void parse();

        protected abstract void validate();

        protected abstract void save();

        public virtual void Generate()
        {
            parse();
            validate();
            save();
        }

    }

    public class PDF : FinalResult
    {
        protected override void parse()
        {
            Console.WriteLine("PDF parsing is successfully Completed");
        }

        protected override void validate()
        {
            Console.WriteLine("PDF Validation is successfully Completed");
        }

        protected override void save()
        {
            Console.WriteLine("PDF Saving is successfully Completed");
        }
    }

    public class Excel : FinalResult
    {
        protected override void parse()
        {
            Console.WriteLine("Excel parsing is successfully Completed");
        }

        protected override void validate()
        {
            Console.WriteLine("Excel Validation is successfully Completed");
        }

        protected override void save()
        {
            Console.WriteLine("Excel Saving is successfully Completed");
        }
    }

    public class Word : FinalResult
    {
        protected override void parse()
        {
            Console.WriteLine("Word parsing is successfully Completed");
        }

        protected override void validate()
        {
            Console.WriteLine("Word Validation is successfully Completed");
        }

        protected override void save()
        {
            Console.WriteLine("Word Saving is successfully Completed");
        }
    }

    public class Test : FinalResult
    {
        protected override void parse()
        {
            Console.WriteLine("Test parsing is successfully Completed");
        }

        protected override void validate()
        {
            Console.WriteLine("Test Validation is successfully Completed");
        }

        protected void Revalidate()
        {
            Console.WriteLine("Test revalidation is successfully Completed");
        }

        protected override void save()
        {
            Console.WriteLine("Test Saving is successfully Completed");
        }

        public override void Generate()
        {
            parse();
            validate();
            Revalidate();
            save();
        }
    }
}
