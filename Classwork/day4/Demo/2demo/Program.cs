﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the Choice");
            Console.WriteLine("1: PDF , 2: Excel , 3:Word");
            int choice = Convert.ToInt32(Console.ReadLine());


            switch (choice)
            {
                case 1:
                    PDF obj = new PDF();
                    obj.parse();
                    obj.validate();
                    obj.save();
                    break;
                case 2:
                    Excel obj1 = new Excel();
                    obj1.parse();
                    obj1.validate();
                    obj1.save();
                    break;
                case 3:
                    Word obj2 = new Word();
                    obj2.parse();
                    obj2.validate();
                    obj2.save();
                    break;
                default:
                    break;
            }
            Console.ReadLine();
        }
    }

    public class PDF
    {
        public void parse()
        {
            Console.WriteLine("PDF parsing is successfully Completed");
        }

        public void validate()
        {
            Console.WriteLine("PDF Validation is successfully Completed");
        }

        public void save()
        {
            Console.WriteLine("PDF Saving is successfully Completed");
        }
    }

    public class Excel
    {
        public void parse()
        {
            Console.WriteLine("Excel parsing is successfully Completed");
        }

        public void validate()
        {
            Console.WriteLine("Excel Validation is successfully Completed");
        }

        public void save()
        {
            Console.WriteLine("Excel Saving is successfully Completed");
        }
    }

    public class Word
    {
        public void parse()
        {
            Console.WriteLine("Word parsing is successfully Completed");
        }

        public void validate()
        {
            Console.WriteLine("Word Validation is successfully Completed");
        }

        public void save()
        {
            Console.WriteLine("Word Saving is successfully Completed");
        }
    }
}
