﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the Choice");
            Console.WriteLine("1: PDF , 2: Excel , 3:Word , 4:Text");
            int choice = Convert.ToInt32(Console.ReadLine());

            Report report = new Report();
            FinalResult obj = report.GetReport(choice);

            obj.Generate();

            Console.ReadLine();
        }
    }

    public class Report
    {
        public FinalResult GetReport(int choice)
        {
            if (choice==1)
            {
                return new PDF();
            }
            else if(choice == 2)
            {
                return new Excel();
            }
            else if (choice == 3)
            {
                return new Word();
            }
            else
            {
                return new Text();
            }
        }
    }

    public abstract class FinalResult
    {
        protected abstract void parse();

        protected abstract void validate();

        protected abstract void save();

        public virtual void Generate()
        {
            parse();
            validate();
            save();
        }

    }

    public class PDF : FinalResult
    {
        protected override void parse()
        {
            Console.WriteLine("PDF parsing is successfully Completed");
        }

        protected override void validate()
        {
            Console.WriteLine("PDF Validation is successfully Completed");
        }

        protected override void save()
        {
            Console.WriteLine("PDF Saving is successfully Completed");
        }
    }

    public class Excel : FinalResult
    {
        protected override void parse()
        {
            Console.WriteLine("Excel parsing is successfully Completed");
        }

        protected override void validate()
        {
            Console.WriteLine("Excel Validation is successfully Completed");
        }

        protected override void save()
        {
            Console.WriteLine("Excel Saving is successfully Completed");
        }
    }

    public class Word : FinalResult
    {
        protected override void parse()
        {
            Console.WriteLine("Word parsing is successfully Completed");
        }

        protected override void validate()
        {
            Console.WriteLine("Word Validation is successfully Completed");
        }

        protected override void save()
        {
            Console.WriteLine("Word Saving is successfully Completed");
        }
    }

    public class Text : FinalResult
    {
        protected override void parse()
        {
            Console.WriteLine("Text parsing is successfully Completed");
        }

        protected override void validate()
        {
            Console.WriteLine("Text Validation is successfully Completed");
        }

        protected void Revalidate()
        {
            Console.WriteLine("Text revalidation is successfully Completed");
        }

        protected override void save()
        {
            Console.WriteLine("Text Saving is successfully Completed");
        }

        public override void Generate()
        {
            parse();
            validate();
            Revalidate();
            save();
        }
    }
}
