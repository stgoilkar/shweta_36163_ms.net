﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DemoMVC.Models;

namespace DemoMVC.Controllers
{
    public class TestController : Controller
    {
        sunbeamDBEntities dbobj = new sunbeamDBEntities();
        // GET: Test
        #region Show hard coded Emp in UI
        //public ActionResult Show()
        //{
        //    Emp emp = new Emp()
        //    {
        //        No = 1,
        //        Name = "Shweta",
        //        Address = "Titwala"
        //    };
        //    return View("XYZ",emp);
        //}
        #endregion

        #region Show hardCoded Emp list in Ui
        //public ActionResult Show()
        //{
        //    List<Emp> emps = new List<Emp>()
        //    {
        //        new Emp{No=1,Name="Shweta",Address="Kolhapur"},
        //        new Emp{No=2,Name="Lalita",Address="Titwala"},
        //        new Emp{No=3,Name="Srushti",Address="Kalyan"},
        //        new Emp{No=4,Name="Prachi",Address="Vada"},
        //        new Emp{No=5,Name="Rutuja",Address="Malvan"},
        //        new Emp{No=6,Name="Aarti",Address="Satara"},
        //    };
        //    return View(emps);
        //} 
        #endregion

        public ActionResult Show()
        {
            try
            {
                var allEmps = dbobj.Emps.ToList();
                return View(allEmps);
            }
            catch(Exception ex)
            {
                return View("Error", ex);
            }
        }

        public ActionResult Update(int Id)
        {
            try
            {
                var EmpToBeUpdated = (from Emp in dbobj.Emps.ToList()
                                      where Emp.Id == Id
                                      select Emp
                                        ).First();

                return View(EmpToBeUpdated);
            }
            catch (Exception ex)
            {
                return View("Error", ex);
            }
        }

        public ActionResult AfterUpdate(FormCollection collection)
        {
            try
            {
                var EmpToBeUpdated = (from Emp in dbobj.Emps.ToList()
                                      where Emp.Id == Convert.ToInt32(collection["Id"])
                                      select Emp
                                     ).First();

                EmpToBeUpdated.Name = collection["Name"].ToString();
                EmpToBeUpdated.Address = collection["Address"].ToString();

                dbobj.SaveChanges();

                return Redirect("/Test/Show");
            }catch(Exception ex)
            {
                return View("Error", ex);
            }
        }

        public ActionResult Delete(int Id)
        {
            try
            {
                var EmpToBeDeleted = (from Emp in dbobj.Emps.ToList()
                                      where Emp.Id == Id
                                      select Emp
                                     ).First();

                dbobj.Emps.Remove(EmpToBeDeleted);
                dbobj.SaveChanges();

                return Redirect("/Test/Show");
            }catch(Exception ex)
            {
                return View("Error", ex);
            }
        }

        public ActionResult Create()
        {
            try
            {
                return View();
            }catch(Exception ex)
            {
                return View("Error", ex);
            }
         
        }

        public ActionResult AfterCreate(FormCollection collection)
        {
            try
            {
                Emp empToBeLoaded = new Emp
                {
                    Id = Convert.ToInt32(collection["Id"]),
                    Name = collection["Name"].ToString(),
                    Address = collection["Address"].ToString()
                };
                dbobj.Emps.Add(empToBeLoaded);
                dbobj.SaveChanges();

                return Redirect("/Test/Show");
            }catch(Exception ex)
            {
                return View("Error", ex);
            }
        }
    }
}