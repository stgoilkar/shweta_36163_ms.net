﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00demo
{
    class Program
    {
        static void Main(string[] args)
        {
            sunbeamDBEntities dbObject = new sunbeamDBEntities();

            #region Select using Entity Framework
            //var allEmp = dbObject.Emps.ToList();

            //foreach (var item in allEmp)
            //{
            //    Console.WriteLine("Employee Name : " + item.Name + " , Address : " + item.Address);
            //}
            //Console.ReadLine();
            #endregion

            #region Insert using Entity Framework
            //dbObject.Emps.Add(new Emp() { Id = 8, Name = "Srushti", Address = "Mumbai" });
            //Console.WriteLine("Added Successfully.....");
            //dbObject.SaveChanges(); 
            #endregion

            #region Update using Entity Framework
            //var empModified = (from Emp in dbObject.Emps.ToList()
            //                   where Emp.Id == 8
            //                   select Emp).First();
            //empModified.Name = "Shweta";
            //empModified.Address = "Kolhapur";
            //Console.WriteLine("Updated Successfully.....");
            //dbObject.SaveChanges();
            #endregion

            #region Delete using Entity Framework
            //var empdelete = (from Emp in dbObject.Emps.ToList()
            //                where Emp.Id==5
            //                select Emp).First();
            //dbObject.Emps.Remove(empdelete);
            //Console.WriteLine("Deleted Successfully....");
            //dbObject.SaveChanges();
            #endregion

            #region Insert using stored Procedure
            //dbObject.spInsert(6, "Rutuja", "Karad");
            #endregion

            #region Calling Updated Book mapping 
            var book = dbObject.Books.ToList();

            foreach (var item in book)
            {
                Console.WriteLine("Book ISBN : " +item.ISBN + " , Title : "+item.Title);
            }
            #endregion
        }
    }
}
